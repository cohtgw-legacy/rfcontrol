library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

--  Uncomment the following lines to use the declarations that are
--  provided for instantiating Xilinx primitive components.
--library UNISIM;
--use UNISIM.VComponents.all;

entity SineCosine is
    Port ( Theta : in std_logic_vector(1 downto 0);
           Clk : in std_logic;
           Sine1 : out std_logic_vector(13 downto 0);
           Cosine1 : out std_logic_vector(13 downto 0);
           Sine2 : out std_logic_vector(13 downto 0);
           Cosine2 : out std_logic_vector(13 downto 0));
end SineCosine;

architecture RTL of SineCosine is

signal sine1Aux, cosine1Aux, sine2Aux, cosine2Aux: signed(13 downto 0);
begin

Sine1 <= std_logic_vector(sine1Aux);
Sine2 <= std_logic_vector(sine2Aux);
Cosine1 <= std_logic_vector(cosine1Aux);
Cosine2 <= std_logic_vector(cosine2Aux);

clocked:process(Clk)
begin
 if Clk'event and Clk='1' then
  case Theta is
      when "00" =>
         sine1Aux   <= to_signed(0, sine1Aux'length);
         cosine1Aux <= to_signed(8191, cosine1Aux'length);
         sine2Aux   <= to_signed(5792, sine2Aux'length);
         cosine2Aux <= to_signed(5792, cosine2Aux'length);
      when "01" =>
         sine1Aux   <= to_signed(8191, sine1Aux'length);
         cosine1Aux <= to_signed(0, cosine1Aux'length);
         sine2Aux   <= to_signed(5792, sine2Aux'length);
         cosine2Aux <= to_signed(-5792, cosine2Aux'length);
      when "10" =>
         sine1Aux   <= to_signed(0, sine1Aux'length);
         cosine1Aux <= to_signed(-8191, cosine1Aux'length);
         sine2Aux   <= to_signed(-5792, sine2Aux'length);
         cosine2Aux <= to_signed(-5792, cosine2Aux'length);
      when "11" =>
         sine1Aux   <= to_signed(-8191, sine1Aux'length);
         cosine1Aux <= to_signed(0, cosine1Aux'length);
         sine2Aux   <= to_signed(-5792, sine2Aux'length);
         cosine2Aux <= to_signed(5792, cosine2Aux'length);
		-- For XST compliance	
      when others =>
         sine1Aux   <= to_signed(0, sine1Aux'length);
         cosine1Aux <= to_signed(8191, cosine1Aux'length);
         sine2Aux   <= to_signed(5792, sine2Aux'length);
         cosine2Aux <= to_signed(5792, cosine2Aux'length);			
   end case;
 end if;

end process clocked;

end RTL;
