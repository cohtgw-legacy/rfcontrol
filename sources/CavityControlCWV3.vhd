----------------------------------------------------
--  
--  Unit Name :  CavityControlCWV3
--
-- Description:
--
-- The CavityControl entity is the top level design for the Virtex II chip on the 
-- LRFSC card.
--
--  Author  :  Javier Serrano
--  Group   :  AB/CO 
--
-- Conventions:
-- xxxN is used to name signal xxx and specify it uses negative logic
-- xxxA is an asynchronous signal
-- xxxC is the xxxA signal after being clocked into the system clock domain
-- No suffix means that the signal is synchronous with the system clock or a derived clock
-- xxxDn is signal xxx delayed by n clock ticks.
-- Ports start with a capital letter, internal signals don't
--
--  Revisions:   
-- 	1.1. (13 January 2005) Initial release
-- 	1.4. (3 June 2008) Clean-up and comments.
--                  
--
--  For any bug or comment, please send an e-mail to Javier.Serrano@cern.ch



library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.RegistersMap.ALL;

entity CavityControlCWV3 is
    Port ( ResetNA        : in  std_logic; -- VME sysreset*
           Clk40In        : in  std_logic; -- System clock
           Clk20In        : in  std_logic; -- External fiducial to guarantee reproducible I,Q streaming
           RefDry         : in std_logic; -- Data Ready output of AD6645 for Reflected signal
           RefOvr         : in std_logic; -- Over Range bit of AD6645 for Reflected signal
           FwdDry         : in std_logic; -- Data Ready output of AD6645 for Forward signal
           FwdOvr         : in std_logic; -- Over range bit of AD6645 for Forward signal
           CavDry         : in std_logic; -- Data Ready output of AD6645 for Cavity signal
           CavOvr         : in std_logic; -- Over range bit of AD6645 for Cavity signal
           RefIn          : in  std_logic_vector(13 downto 0); -- Reflected signal from AD6645
           FwdIn          : in  std_logic_vector(13 downto 0); -- Forward signal from AD6645
           CavIn          : in  std_logic_vector(13 downto 0); -- Cavity signal from AD6645
           ResCtrlD       : out std_logic; -- Serial data output for MAX5200
           ResSClk        : out std_logic; -- Serial clock output for MAX5200
           ResCSN         : out std_logic; -- Load DAC output for MAX5200, negative logic
           RFDAC1         : out std_logic_vector(13 downto 0); -- Time muxed output data for AD9755
           RFDAC2         : out std_logic_vector(13 downto 0); -- Time muxed output data for AD9755
           SwControl      : out std_logic_vector(3 downto 0); -- Control for on-board RF switches
           XProtectInNA   : in std_logic; -- Fast protect signal input
           XProtectOutN   : out std_logic; -- Fast protect signal output
           XIrq2N         : out std_logic; -- VME IRQ, level 2
           XDtackN        : out std_logic; -- Data Transfer Acknowledge for VME
           XMuxCtrl       : out std_logic; -- Mux control to protect VME from unconfigured FPGA
           XIackInNA      : in std_logic; -- Interrupt Acknowledge Daisy Chain input
           XIackOut       : out std_logic; -- Interrupt Aknowledge Daisy Chain output
           VMEDir         : out std_logic; -- Control of VME bidirectionnal buffers
           VMEOE          : out std_logic; -- Output Enable for VME buffers
           VMEData        : inout std_logic_vector(15 downto 0); -- VME Data Bus
           VMEAddress     : in std_logic_vector(23 downto 1); -- VME Address Bus
           VMEAM          : in std_logic_vector(5 downto 0); -- VME Address Modifiers
           VMElwordNA     : in std_logic; -- VME long word signal
           VMEASNA        : in std_logic; -- VME Address Strobe
           VMEDS1NA       : in std_logic; -- VME Data Strobe 1
           VMEIackNA      : in std_logic; -- VME Interrupt Acknowledge line
           VMEDS0NA       : in std_logic; -- VME Data Strobe 0
           VMEWriteNA     : in std_logic; -- VME Write signal
           VMESysClk      : in std_logic; -- VME System clock

           StartCycleA    : in std_logic; -- Start Cycle TTL input
           RFOnA          : in std_logic; -- RF ON TTL input
    
	        -- Interfaces for the 8 RAM chips (IDT71V35781)
           -- Diagnostics channel 1
			  -- RAM1 and AD9761 DIAG1 DAC share the Data Bus
           RAM1Addr       : out std_logic_vector(17 downto 0); -- RAM1 address bus (shared with RAM2, RAM3 and RAM4)
           RAM1Data       : inout std_logic_vector(15 downto 0); -- RAM1 and DIAG1 data bus
           IQSel1         : out std_logic; -- I/Q select for DIAG1 DAC
           RAM1CSN        : out std_logic; -- RAM1 Chip Select
           RAM1OEN        : out std_logic; -- RAM1 Output Enable
           RAM1WEN        : out std_logic; -- RAM1 Write Enable

           -- Diagnostics channel 2
			  -- RAM2 and AD9761 DIAG2 DAC share the Data Bus
           RAM2Data       : inout std_logic_vector(15 downto 0); -- RAM2 and DIAG2 Data bus
           IQSel2         : out std_logic; -- I/Q select for DIAG2 DAC
           RAM2CSN        : out std_logic; -- RAM2 Chip Select
           RAM2OEN        : out std_logic; -- RAM2 Output Enable
           RAM2WEN        : out std_logic; -- RAM2 Write Enable
     
           -- Diagnostics channel 3
			  -- RAM3 and AD9761 DIAG3 DAC share the Data Bus
           RAM3Data       : inout std_logic_vector(15 downto 0); -- RAM3 and DIAG3 Data bus
           IQSel3         : out std_logic; -- I/Q select for DIAG3 DAC
           RAM3CSN        : out std_logic; -- RAM3 Chip Select
           RAM3OEN        : out std_logic; -- RAM3 Output Enable
           RAM3WEN        : out std_logic; -- RAM3 Write Enable
   
           -- Diagnostics channel 4
			  -- RAM4 and AD9761 DIAG4 DAC share the Data Bus			  
           RAM4Data       : inout std_logic_vector(15 downto 0); -- RAM4 and DIAG4 Data bus
           IQSel4         : out std_logic; -- I/Q select for DIAG4 DAC
           RAM4CSN        : out std_logic; -- RAM4 Chip Select
           RAM4OEN        : out std_logic; -- RAM4 Output Enable
           RAM4WEN        : out std_logic; -- RAM4 Write Enable
      
           -- Learning RAM A (not used)
           RAM5Addr       : out std_logic_vector(17 downto 0);
           RAM5Data       : out std_logic_vector(15 downto 0);
           RAM5CSN        : out std_logic;
           RAM5OEN        : out std_logic;
           RAM5WEN        : out std_logic;

           -- Learning RAM B (not used)
           RAM6Addr       : out std_logic_vector(17 downto 0);
           RAM6Data       : out std_logic_vector(15 downto 0);
           RAM6CSN        : out std_logic;
           RAM6OEN        : out std_logic;
           RAM6WEN        : out std_logic;
        
           -- Set Points RAM
           RAM7Addr       : out std_logic_vector(17 downto 0);
           RAM7Data       : inout std_logic_vector(15 downto 0);
           RAM7CSN        : out std_logic;
           RAM7OEN        : out std_logic;
           RAM7WEN        : out std_logic;

           -- Feedforward RAM 
           RAM8Addr       : out std_logic_vector(17 downto 0);
           RAM8Data       : inout std_logic_vector(15 downto 0);
           RAM8CSN        : out std_logic;
           RAM8OEN        : out std_logic;
           RAM8WEN        : out std_logic;

           RAMZZ          : out std_logic_vector(8 downto 1); -- sleep for RAMs
           DIAGZZ         : out std_logic_vector(4 downto 1); -- sleep for Diagnostics DACs (AD9761)
           
			  -- LEDs, there are lit by driving a '1'
           Led1           : out std_logic;
           Led2           : out std_logic;
           Led3           : out std_logic;
           Led4           : out std_logic;
           Led5           : out std_logic;
			  
			  -- VME base address is ADSwitch(7 downto 4)
           ADSwitch       : in std_logic_vector(7 downto 0);    -- on-board DIP switches
			  
			  -- Tests
           TestOut        : out std_logic_vector(17 downto 0) -- test header
           );
end CavityControlCWV3;

architecture RTL of CavityControlCWV3 is

attribute syn_useioff : boolean;
attribute syn_useioff of RTL : architecture is true;

COMPONENT vme_intfce

    generic ( AddrWidth  : integer:=24;
	  BaseAddrWidth   : integer:=4;
	  DataWidth       : integer:=16;
     DirSamePolarity  : std_logic:='0';
     UnalignDataWidth : integer:=8;
     InterruptEn      : std_logic:='1');
 
  port (
        ResetNA       : in std_logic;
        Clk           : in std_logic;
        VmeAddrA      : in std_logic_vector(AddrWidth-1 downto 1 );
        VmeAsNA       : in std_logic;
        VmeDs1NA      : in std_logic;
        VmeDs0NA      : in std_logic;
        VmeData       : inout std_logic_vector(DataWidth-1 downto 0 );
        VmeDataUnAlign: inout std_logic_vector(UnalignDataWidth-1 downto 0); 
        VmeDir        : out std_logic;
        VmeDirFloat   : out std_logic;
        VmeBufOeN     : out std_logic;
        VmeWriteNA    : in std_logic;
        VmeLwordNA    : in std_logic;
        VmeIackNA     : in std_logic;
        IackOutNA     : out std_logic;
        IackInNA      : in std_logic;
        VmeIntReqN    : out std_logic_vector (7 downto 1);
        vmeDtackN     : out std_logic;
        ModuleAddr    : in std_logic_vector(BaseAddrWidth-1 downto 0 );
        VmeAmA        : in std_logic_vector(4 downto 0 );

        AddrMem       : out std_logic_vector(AddrWidth-BaseAddrWidth-1 downto 0 );
        ReadMem       : out std_logic;
        WriteMem      : out std_logic;
        DataFromMemValid : in std_logic;
        DataFromMem   : in std_logic_vector(DataWidth-1 downto 0 );
        DataToMem     : out std_logic_vector(DataWidth-1 downto 0 );
        IntProcessed  : out std_logic;
        UserIntReqN   : in std_logic;
        UserBlocks    : in std_logic;
        OpFinishedOut : out std_logic;
	     IRQLevelReg   : in std_logic_vector (3 downto 1);
	     IRQStatusIDReg: in std_logic_vector (DataWidth-1 downto 0);
        VmeState      : out std_logic_vector (3 downto 0));
END COMPONENT;

COMPONENT irqcontrol
	PORT(
		Clk : IN std_logic;
		IrqEnable : IN std_logic;
		RFOn : IN std_logic;
		StartCycleA: IN std_logic;
		StartCycle: out std_logic;
		Reset : IN std_logic;
		WriteVMEIRQR : IN std_logic;
		ReadIRQSRCR: in std_logic;
		DataFromDecoder : IN std_logic_vector(15 downto 0);
		IntProcessed : IN std_logic;          
		VMEIRQR : OUT std_logic_vector(15 downto 0);
		IRQSRCR : OUT std_logic_vector(15 downto 0);
		RFOnRising : OUT std_logic;
		RFOnFalling : OUT std_logic;
      StateOut: out std_logic_vector(2 downto 0);
		IrqN : OUT std_logic
		);
END COMPONENT;

COMPONENT clockmanager
	PORT(
		Reset : IN std_logic;
		Clk40 : IN std_logic;          
		Clk40Int : OUT std_logic;
		Clk80Int : OUT std_logic;
		Clk80IntN : OUT std_logic;
		Lock40Int : OUT std_logic
		);
END COMPONENT;

COMPONENT iqcounter
	PORT(
		Clk20 : IN std_logic;
		Clk80 : IN std_logic;    
      Alarm : out std_logic;
		IQCount : OUT std_logic_vector(1 downto 0)
		);
END COMPONENT;

COMPONENT iqdemodcw
	PORT(
		DataInA : IN std_logic_vector(13 downto 0);
		Clk80In : IN std_logic;
      IQCount : IN std_logic_vector(1 downto 0);          
		IData : OUT std_logic_vector(13 downto 0);
		QData : OUT std_logic_vector(13 downto 0)
		);
END COMPONENT;

	COMPONENT mul2x2
	PORT(
		IDataIn : IN std_logic_vector(13 downto 0);
		QDataIn : IN std_logic_vector(13 downto 0);
		WriteIA : IN std_logic;
		WriteIB : IN std_logic;
		WriteQA : IN std_logic;
		WriteQB : IN std_logic;
		DataFromDecoder : IN std_logic_vector(15 downto 0);
		Clk40 : IN std_logic;
		Reset : IN std_logic;          
		IFactorA : OUT std_logic_vector(15 downto 0);
		IFactorB : OUT std_logic_vector(15 downto 0);
		QFactorA : OUT std_logic_vector(15 downto 0);
		QFactorB : OUT std_logic_vector(15 downto 0);
		IDataOut : OUT std_logic_vector(13 downto 0);
		QDataOut : OUT std_logic_vector(13 downto 0)
		);
	END COMPONENT;

COMPONENT IBUFG
      port (I: in std_logic; O: out std_logic);
END COMPONENT; 

COMPONENT diagnostics
	PORT(
		Clk40 : IN std_logic;
		Clk80 : IN std_logic;
		SyncReset : IN std_logic;
		RFONRising : IN std_logic;
		RFONFalling : IN std_logic;
	   ProdLocalMode: in std_logic;
	   ProdRemoteMode: in std_logic;
		RefI : IN std_logic_vector(13 downto 0);
		RefQ : IN std_logic_vector(13 downto 0);
		FwdI : IN std_logic_vector(13 downto 0);
		FwdQ : IN std_logic_vector(13 downto 0);
		CavI : IN std_logic_vector(13 downto 0);
		CavQ : IN std_logic_vector(13 downto 0);
		ErrI : IN std_logic_vector(13 downto 0);
		ErrQ : IN std_logic_vector(13 downto 0);
		OutI : IN std_logic_vector(13 downto 0);
		OutQ : IN std_logic_vector(13 downto 0);
		AddrFromRM : IN std_logic_vector(17 downto 0);
		RMReadDiag1 : IN std_logic;
		RMReadDiag2 : IN std_logic;
		RMReadDiag3 : IN std_logic;
		RMReadDiag4 : IN std_logic;
		DiagRequest : IN std_logic;
		WriteDiag1Select : IN std_logic;
		WriteDiag2Select : IN std_logic;
		WriteDiag3Select : IN std_logic;
		WriteDiag4Select : IN std_logic;
		DataFromDecoder : IN std_logic_vector(15 downto 0);    
		RAM1Data : INOUT std_logic_vector(15 downto 0);
		RAM2Data : INOUT std_logic_vector(15 downto 0);
		RAM3Data : INOUT std_logic_vector(15 downto 0);
		RAM4Data : INOUT std_logic_vector(15 downto 0);      
		DataToRM : OUT std_logic_vector(15 downto 0);
		DataToRMValid : OUT std_logic;
		RAMAddr : OUT std_logic_vector(17 downto 0);
		IQSEl1 : OUT std_logic;
		IQSEl2 : OUT std_logic;
		IQSEl3 : OUT std_logic;
		IQSEl4 : OUT std_logic;
		RAM1CS : OUT std_logic;
		RAM1OE : OUT std_logic;
		RAM1WE : OUT std_logic;
		RAM2CS : OUT std_logic;
		RAM2OE : OUT std_logic;
		RAM2WE : OUT std_logic;
		RAM3CS : OUT std_logic;
		RAM3OE : OUT std_logic;
		RAM3WE : OUT std_logic;
		RAM4CS : OUT std_logic;
		RAM4OE : OUT std_logic;
		RAM4WE : OUT std_logic;
		Diag1Select : OUT std_logic_vector(15 downto 0);
		Diag2Select : OUT std_logic_vector(15 downto 0);
		Diag3Select : OUT std_logic_vector(15 downto 0);
		Diag4Select : OUT std_logic_vector(15 downto 0)
		);
END COMPONENT;  

COMPONENT rescontrol
	PORT(
		Reset : IN std_logic;
		Clk40 : IN std_logic;
		WriteResCtrl : IN std_logic;
		WriteResCtrlTime: in std_logic;
		ResCtrlTime: out std_logic_vector(15 downto 0);
		CavI: in std_logic_vector(13 downto 0);
		CavQ: in std_logic_vector(13 downto 0);
		FwdI: in std_logic_vector(13 downto 0);
		FwdQ: in std_logic_vector(13 downto 0);	
      RFOnRising: in std_logic;
      ResFwdI: out std_logic_vector(15 downto 0);
      ResFwdQ: out std_logic_vector(15 downto 0);
      ResCavI: out std_logic_vector(15 downto 0);
      ResCavQ: out std_logic_vector(15 downto 0);
		DataFromDecoder : IN std_logic_vector(15 downto 0); 
      TimeTracker: out std_logic_vector(15 downto 0);		
		RESCTRL : OUT std_logic_vector(15 downto 0);
		CSN : OUT std_logic;
      ResCtrlD: out std_logic;
      ResSClk: out std_logic
		);
END COMPONENT;

COMPONENT iqmodulator
	PORT(
		resetna : IN std_logic;
		clk : IN std_logic;
		ConfigMode: in std_logic;
		i : IN std_logic_vector(13 downto 0);
		q : IN std_logic_vector(13 downto 0);  
      IQCount : in std_logic_vector(1 downto 0);        
		waveout1 : OUT std_logic_vector(13 downto 0);
		waveout2 : OUT std_logic_vector(13 downto 0)
		);
END COMPONENT;

COMPONENT picontrollercw
	PORT(
		SyncReset : IN std_logic;
		Clk40 : IN std_logic;
		DataFromDecoder : IN std_logic_vector(15 downto 0);
		WriteKP : IN std_logic;
		WriteKI : IN std_logic;
		RFONRising : IN std_logic;
		RFONFalling : IN std_logic;
      CWMode: in std_logic;
      AntiWindUp: in std_logic;
		ConfigMode : IN std_logic;
		DataIn : IN std_logic_vector(13 downto 0);
		SetPoints : IN std_logic_vector(13 downto 0);
		SetPointsValid : IN std_logic;          
		DataOut : OUT std_logic_vector(13 downto 0);
      DataOutSat: out std_logic_vector(28 downto 0);
		KP : OUT std_logic_vector(15 downto 0);
		KI : OUT std_logic_vector(15 downto 0);
		PropOut : OUT std_logic_vector(13 downto 0);
		IntOut : OUT std_logic_vector(13 downto 0);
      LongIntOut: out std_logic_vector(26 downto 0);
		ErrorOut : OUT std_logic_vector(13 downto 0)
		);
END COMPONENT;

COMPONENT feedforward
	PORT(
		Clk40 : IN std_logic;
		SyncReset : IN std_logic;
		DataIn : IN std_logic_vector(13 downto 0);
		RFONFalling : IN std_logic;
      RFONRising: in std_logic;
		MainLoopSwitch : IN std_logic;
		FFData : IN std_logic_vector(13 downto 0);
		FFDataValid : IN std_logic;          
		DataOut : OUT std_logic_vector(13 downto 0)
		);
END COMPONENT;

component TimingManager 
    Port ( SyncReset : in  STD_LOGIC;
           Clk : in  STD_LOGIC;
           StartCycle : in  STD_LOGIC;
           RFOnRising : in  STD_LOGIC;
           WriteNCycle : in  STD_LOGIC;
			  DataFromDecoder: in std_logic_vector(15 downto 0);
           NextCycle : out  STD_LOGIC_VECTOR (15 downto 0);
           PresentCycle : out  STD_LOGIC_VECTOR (15 downto 0);
           PulseNumber : out  STD_LOGIC_VECTOR (15 downto 0));
end component;

COMPONENT snapshots
	PORT(
		SyncReset : IN std_logic;
		Clk40 : IN std_logic;
		DataFromDecoder : IN std_logic_vector(15 downto 0);
		WriteDiagTime : IN std_logic;
		RFOnRising : IN std_logic;
		RFOnFalling : IN std_logic;
		ConfigMode : IN std_logic;
		TimeTracker: in std_logic_vector(15 downto 0);
		RefIIn : IN std_logic_vector(13 downto 0);
		RefQIn : IN std_logic_vector(13 downto 0);
		FwdIIn : IN std_logic_vector(13 downto 0);
		FwdQIn : IN std_logic_vector(13 downto 0);
		CavIIn : IN std_logic_vector(13 downto 0);
		CavQIn : IN std_logic_vector(13 downto 0);
		ErrIIn : IN std_logic_vector(13 downto 0);
		ErrQIn : IN std_logic_vector(13 downto 0);
		IntIIn : IN std_logic_vector(13 downto 0);
		IntQIn : IN std_logic_vector(13 downto 0);
		PropIIn : IN std_logic_vector(13 downto 0);
		PropQIn : IN std_logic_vector(13 downto 0);
		OutIIn : IN std_logic_vector(13 downto 0);
		OutQIn : IN std_logic_vector(13 downto 0);          
		DiagTime : OUT std_logic_vector(15 downto 0);
		RFOffTime : OUT std_logic_vector(15 downto 0);
		RefIOut : OUT std_logic_vector(15 downto 0);
		RefQOut : OUT std_logic_vector(15 downto 0);
		FwdIOut : OUT std_logic_vector(15 downto 0);
		FwdQOut : OUT std_logic_vector(15 downto 0);
		CavIOut : OUT std_logic_vector(15 downto 0);
		CavQOut : OUT std_logic_vector(15 downto 0);
		ErrIOut : OUT std_logic_vector(15 downto 0);
		ErrQOut : OUT std_logic_vector(15 downto 0);
		IntIOut : OUT std_logic_vector(15 downto 0);
		IntQOut : OUT std_logic_vector(15 downto 0);
		PropIOut : OUT std_logic_vector(15 downto 0);
		PropQOut : OUT std_logic_vector(15 downto 0);
		OutIOut : OUT std_logic_vector(15 downto 0);
		OutQOut : OUT std_logic_vector(15 downto 0)
		);
END COMPONENT;

COMPONENT vmedecoder
	PORT(
		Clk40 : IN std_logic;
		ResetNA : IN std_logic;
		AddrFromVME : IN std_logic_vector(18 downto 0);
		DataFromVME : IN std_logic_vector(15 downto 0);
		WriteFromVME : IN std_logic;
		ReadFromVME : IN std_logic;
		DataFromRAM : IN std_logic_vector(15 downto 0);
		DataFromRAMValid : IN std_logic;
		Regs : IN RegsType;          
		DataToVME : OUT std_logic_vector(15 downto 0);
		DataToVMEValid : OUT std_logic;
		DataFromDecoder : OUT std_logic_vector(15 downto 0);
		AddrFromDecoder : OUT std_logic_vector(18 downto 0);
		WriteRegs : OUT std_logic_vector(NumberOfRegs-1 downto 0);
		ReadRegs : OUT std_logic_vector(NumberOfRegs-1 downto 0);
		WriteRAM : OUT std_logic;
		ReadRAM : OUT std_logic
		);
END COMPONENT;

COMPONENT rammanager
	PORT(
		Clk40 : IN std_logic;
		SyncReset : IN std_logic;
		VMEWriteRAM : IN std_logic;
		VMEReadRAM : IN std_logic;
		AddrFromDecoder : IN std_logic_vector(18 downto 0);
		DataFromDecoder : IN std_logic_vector(15 downto 0);
		WriteRAMSELECT : IN std_logic;
		SPDataToRM : IN std_logic_vector(15 downto 0);
		SPDataValid : IN std_logic;
		FFDataToRM : IN std_logic_vector(15 downto 0);
		FFDataValid : IN std_logic;
		DiagDataToRM : IN std_logic_vector(15 downto 0);
		DiagDataValid : IN std_logic;          
		RMDataToDecoder : OUT std_logic_vector(15 downto 0);
		RMDataToDecoderValid : OUT std_logic;
		RAMSELECT : OUT std_logic_vector(15 downto 0);
		RMDataToRAM : OUT std_logic_vector(15 downto 0);
		RMAddrToRAM : OUT std_logic_vector(17 downto 0);
		ReadSPRAM : OUT std_logic;
		WriteSPRAM : OUT std_logic;
		ReadFFRAM : OUT std_logic;
		WriteFFRAM : OUT std_logic;
		ReadDIAG1RAM : OUT std_logic;
		ReadDIAG2RAM : OUT std_logic;
		ReadDIAG3RAM : OUT std_logic;
		ReadDIAG4RAM : OUT std_logic
		);
END COMPONENT;

COMPONENT setpoints
	PORT(
		SyncReset : IN std_logic;
		Clk40 : IN std_logic;
		AddrFromRM : IN std_logic_vector(17 downto 0);
		DataFromRM : IN std_logic_vector(15 downto 0);
		ReadFromRM : IN std_logic;
		WriteFromRM : IN std_logic;
		RFONFalling : IN std_logic;
		RFONRising : IN std_logic;
	   PresentCycle: in std_logic_vector(4 downto 0);
		ConfigMode : IN std_logic;    
		RAMData : INOUT std_logic_vector(15 downto 0);      
		DataToRM : OUT std_logic_vector(15 downto 0);
		DataToRMValid : OUT std_logic;
		IDataToProcess : OUT std_logic_vector(15 downto 0);
		QDataToProcess : OUT std_logic_vector(15 downto 0);
		DataToProcessValid : OUT std_logic;
		RAMAddress : OUT std_logic_vector(17 downto 0);
		RAMOE : OUT std_logic;
		RAMCS : OUT std_logic;
		RAMWE : OUT std_logic
		);
END COMPONENT;

COMPONENT commands
	PORT(
		Clk40 : IN std_logic;
		ResetNA : IN std_logic;
		IQAlarm : in std_logic;
		CavOvr: in std_logic;
		FwdOvr: in std_logic;
	   RefOvr: in std_logic;
		RFOnRising : IN std_logic;
		RFOnFalling : IN std_logic;
		FProtect : IN std_logic;
		WriteCONTROLR : IN std_logic;
		WriteSWITCHCTRLR : IN std_logic;
		WriteSOFTSWITCHR : IN std_logic;
		WriteSTATER: IN std_logic;
		WriteRFONMAXLENGTHR: in std_logic;
		ReadStatus: in std_logic;
	   TimeTracker: in std_logic_vector(15 downto 0);
		DataFromDecoder : IN std_logic_vector(15 downto 0);          
		CavSwitch : OUT std_logic;
		RefSwitch : OUT std_logic;
		FwdSwitch : OUT std_logic;
		OutSwitch : OUT std_logic;
		MainLoopSwitch : OUT std_logic;
      SatDisabled: out std_logic;
		DoAcquisition : OUT std_logic;
		ConfigMode : OUT std_logic;
	   ProdLocalMode: out std_logic;
		ProdRemoteMode: out std_logic;
		CWMode: out std_logic;
		CONTROLR : OUT std_logic_vector(15 downto 0);
		STATER : OUT std_logic_vector(15 downto 0); 
		STATUSR : OUT std_logic_vector(15 downto 0);
		RFONMAXLENGTHR: out std_logic_vector(15 downto 0);
		SWITCHCTRLR : OUT std_logic_vector(15 downto 0);
		SOFTSWITCHR : OUT std_logic_vector(15 downto 0)
		);
END COMPONENT;

COMPONENT satcontrolcw
	PORT(
		Clk : IN std_logic;
		IDataIn : IN std_logic_vector(13 downto 0);
		QDataIn : IN std_logic_vector(13 downto 0);
		ISatIn : IN std_logic_vector(28 downto 0);
		QSatIn : IN std_logic_vector(28 downto 0);          
		Saturated : OUT std_logic;
		Iout : OUT std_logic_vector(13 downto 0);
		Qout : OUT std_logic_vector(13 downto 0)
		);
END COMPONENT;

COMPONENT oneshot
	PORT(
		Clk : IN std_logic;
		ResetNA : IN std_logic;
		PulseIn : IN std_logic;          
		PulseOut : OUT std_logic
		);
END COMPONENT;



-- Signals declaration
signal clk20, clk20A, clk40, clk40Aux, clk80, clk80N : std_logic;
signal lock40Int : std_logic;
signal iqCount: std_logic_vector(1 downto 0);
signal refI, refQ, fwdI, fwdQ, cavI, cavQ: std_logic_vector(13 downto 0);
signal refIR, refQR, fwdIR, fwdQR, cavIR, cavQR: std_logic_vector(13 downto 0);
signal cavIC, cavQC: std_logic_vector(13 downto 0);
signal cavISat, cavQSat: std_logic_vector(28 downto 0);
signal resetA: std_logic;
signal vmeIntReqAux: std_logic_vector(7 downto 1);
signal vmeAMAux: std_logic_vector(4 downto 0);
signal vmeAddrInt: std_logic_vector(19 downto 0);
signal readFromVME, writeFromVME, dataToVMEValid: std_logic;
signal irqtoVMEN, intProcessed: std_logic;
signal dataToVME, dataFromVME: std_logic_vector(15 downto 0);
signal syncReset: std_logic;
signal rfOnToggle: std_logic;
signal dataFromDecoder: std_logic_vector(15 downto 0);
signal addrFromDecoder: std_logic_vector(18 downto 0);
signal writeRegs, readRegs: std_logic_vector(NumberOfRegs-1 downto 0);
signal writeRAM, readRAM: std_logic;
signal registers: RegsType;
signal rfOnRising, rfOnFalling: std_logic;
signal cavSwitch, refSwitch, fwdSwitch, outSwitch, mainLoopSwitch: std_logic;
signal doAcquisition, configMode: std_logic;
signal rmDataToDecoder, rmDataToRAM, spDataToRM, ffDataToRM, diagDataToRM: std_logic_vector(15 downto 0);
signal rmDataToDecoderValid: std_logic;
signal rmAddrToRAM: std_logic_vector(17 downto 0);
signal readSPRAM, writeSPRAM, spDataValid: std_logic;
signal readFFRAM, writeFFRAM, ffDataValid: std_logic;
signal diagDataValid, readDiag1RAM, readDiag2RAM, readDiag3RAM, readDiag4RAM: std_logic;
signal spIDataToPI, spQDataToPI, ffIDataToLoop, ffQDataToLoop: std_logic_vector(15 downto 0);
signal spDataToPIValid, ffDataToLoopValid: std_logic;
signal cavIInt, cavIProp, cavIErr, cavIFF: std_logic_vector(13 downto 0);
signal cavQInt, cavQProp, cavQErr, cavQFF: std_logic_vector(13 downto 0);
signal debounceState: std_logic_vector(2 downto 0);
signal ram1AddrAux: std_logic_vector(17 downto 0);
signal ram1OEAux, ram1CSAux, ram1WEAux, iqSel1Aux: std_logic;
signal saturated, badlySaturated: std_logic;
signal cavIFixed, cavQFixed: std_logic_vector(13 downto 0);
signal cavIToMod, cavQToMod: std_logic_vector(13 downto 0);
signal satDisabled: std_logic;
signal cavILongInt, cavQLongInt: std_logic_vector(26 downto 0);
signal cwMode, antiWindUp: std_logic;
signal resetC, prodLocalMode, prodRemoteMode, startCycleRising: std_logic;
signal timeTracker: std_logic_vector(15 downto 0);
signal cavOvrC, fwdOvrC, refOvrC, iqAlarm: std_logic;
signal resCSNAux: std_logic;


begin

antiWindUp <= not registers(CONTROLR)(2);

-- VHDL date registers: 20 June 2008
registers(VHDLVERHR) <= X"485B"; 
registers(VHDLVERLR) <= X"D7B4";

RAM1Addr <= ram1AddrAux;
RAM1OEN <= ram1OEAux;
RAM1CSN <= ram1CSAux;
RAM1WEN <= ram1WEAux;
IQSel1 <= iqSel1Aux;
ResCSN <= resCSNAux;


XIrq2N <= vmeIntReqAux(2);
vmeAMAux <= VMEAM(5 downto 3) & VMEAM(1 downto 0);

clk20A <= Clk20In;
resetA <= not ResetNA;

Clk40IBUFGDS: IBUFG port map (I => Clk40In, O => clk40Aux);

-- The purpose of this process is just to synchronize the clk20A signal.
-- By sampling with the clk40 clock, we will just have to take care of the setup/hold time of the
-- clk40A input in the constraints editor once.
Sync20: process(clk40)
begin
  if clk40'event and clk40='1' then
   clk20 <= clk20A;
  end if;
end process Sync20;

SyncResetProcess: process(clk40)
begin
  if clk40'event and clk40='1' then
   resetC <= resetA;
   syncReset <= resetC;
  end if;
end process SyncResetProcess;

ClkManager: clockmanager PORT MAP(
		Reset => resetA,
		Clk40 => clk40Aux,
		Clk40Int => clk40,
		Clk80Int => clk80,
		Clk80IntN => clk80N,
		Lock40Int => lock40Int
	);

U0_vme_intfce: vme_intfce 
 GENERIC MAP ( 
      AddrWidth => 24,
	   BaseAddrWidth => 4,
	   DataWidth => 16,
      DirSamePolarity  => '0',
      InterruptEn => '1')

PORT MAP(
		resetna => ResetNA,     
		clk => clk40, 
		vmeaddra => VMEAddress, 
		vmeasna => VMEASNA,  
		vmeds1na => VMEDS1NA,
		vmeds0na => VMEDS0NA,
		vmedata => VMEData, 
--		vmedataunalign => ,
		vmedir => VMEDir,
--		vmedirfloat => ,
		vmebufoen => VMEOE,
		vmewritena => VMEWriteNA,
		vmelwordna => VMElwordNA,
		vmeiackna => VMEIackNA,
		iackoutna => XIackOut,
		iackinna => XIackInNA,
		vmeintreqn => vmeIntReqAux,
		vmedtackn => XDtackN,
		moduleaddr => ADSwitch(7 downto 4),
		vmeama => vmeAMAux,
		addrmem => vmeAddrInt,
		readmem => readFromVME,
		writemem => writeFromVME,
		datafrommemvalid => dataToVMEValid,
		datafrommem => dataToVME,
		datatomem => dataFromVME,
		intprocessed => intProcessed,
		userintreqn => irqToVMEN,
		userblocks => '0',
		opfinishedout => open,
		irqlevelreg => "010",
		irqstatusidreg => registers(VMEIRQR),
		vmestate => open
	);



Inst_vmedecoder: vmedecoder PORT MAP(
		Clk40 => clk40,
		ResetNA => ResetNA,
		AddrFromVME => vmeAddrInt(19 downto 1), -- we address words inside the decoder, not bytes
		DataFromVME => dataFromVME,
		DataToVME => dataToVME,
		DataToVMEValid => dataToVMEValid,
		WriteFromVME => writeFromVME,
		ReadFromVME => readFromVME,
		DataFromDecoder => dataFromDecoder,
		AddrFromDecoder => addrFromDecoder,
		WriteRegs => writeRegs,
		ReadRegs => readRegs,
		DataFromRAM => rmDataToDecoder,
		DataFromRAMValid => rmDataToDecoderValid,
		WriteRAM => writeRAM,
		ReadRAM => readRAM,
		Regs => registers
	);

U0_rammanager: rammanager PORT MAP(
		Clk40 => clk40,
		SyncReset => syncReset,
		VMEWriteRAM => writeRAM,
		VMEReadRAM => readRAM,
		AddrFromDecoder => addrFromDecoder,
		DataFromDecoder => dataFromDecoder,
		RMDataToDecoder => rmDataToDecoder,
		RMDataToDecoderValid => rmDataToDecoderValid,
		WriteRAMSELECT => writeRegs(RAMSELECTR),
		RAMSELECT => registers(RAMSELECTR),
		RMDataToRAM => rmDataToRAM,
		RMAddrToRAM => rmAddrToRAM,
		ReadSPRAM => readSPRAM,
		WriteSPRAM => writeSPRAM,
		SPDataToRM => spDataToRM,
		SPDataValid => spDataValid,
		ReadFFRAM => readFFRAM,
		WriteFFRAM => writeFFRAM,
		FFDataToRM => ffDataToRM,
		FFDataValid => ffDataValid,
		DiagDataToRM => diagDataToRM,
		DiagDataValid => diagDataValid,
		ReadDIAG1RAM => readDiag1RAM,
		ReadDIAG2RAM => readDiag2RAM,
		ReadDIAG3RAM => readDiag3RAM,
		ReadDIAG4RAM => readDiag4RAM
	);

U0_setpoints: setpoints PORT MAP(
		SyncReset => syncReset,
		Clk40 => clk40,
		AddrFromRM => rmAddrToRAM,
		DataFromRM => rmDataToRAM,
		ReadFromRM => readSPRAM,
		WriteFromRM => writeSPRAM,
		DataToRM => spDataToRM,
		DataToRMValid => spDataValid,
		RFONFalling => rfOnFalling,
		RFONRising => rfOnRising,
		PresentCycle => registers(PRESENTCYCLER)(4 downto 0),
		ConfigMode => configMode,
		IDataToProcess => spIDataToPI,
		QDataToProcess => spQDataToPI,
		DataToProcessValid => spDataToPIValid,
		RAMAddress => RAM7Addr,
		RAMData => RAM7Data,
		RAMOE => RAM7OEN,
		RAMCS => RAM7CSN,
		RAMWE => RAM7WEN
	);

U1_setpoints: setpoints PORT MAP(
		SyncReset => syncReset,
		Clk40 => clk40,
		AddrFromRM => rmAddrToRAM,
		DataFromRM => rmDataToRAM,
		ReadFromRM => readFFRAM,
		WriteFromRM => writeFFRAM,
		DataToRM => ffDataToRM,
		DataToRMValid => ffDataValid,
		RFONFalling => rfOnFalling,
		RFONRising => rfOnRising,
		PresentCycle => registers(PRESENTCYCLER)(4 downto 0),
		ConfigMode => configMode,
		IDataToProcess => ffIDataToLoop,
		QDataToProcess => ffQDataToLoop,
		DataToProcessValid => ffDataToLoopValid,
		RAMAddress => RAM8Addr,
		RAMData => RAM8Data,
		RAMOE => RAM8OEN,
		RAMCS => RAM8CSN,
		RAMWE => RAM8WEN
	);

	Inst_diagnostics: diagnostics PORT MAP(
		Clk40 => clk40,
      Clk80 => clk80,
		SyncReset => syncReset,
		RFONRising => rfOnRising,
		RFONFalling => rfOnFalling,
		ProdLocalMode => prodLocalMode,
	   ProdRemoteMode => prodRemoteMode,
		RefI => refIR,
		RefQ => refQR,
		FwdI => fwdIR,
		FwdQ => fwdQR,
		CavI => cavIR,
		CavQ => cavQR,
		ErrI => cavIErr,
		ErrQ => cavQErr,
		OutI => cavIToMod,
		OutQ => cavQToMod,
		AddrFromRM => rmAddrToRAM,
		DataToRM => diagDataToRM,
		DataToRMValid => diagDataValid,
		RAMAddr => ram1AddrAux,
		RAM1Data => RAM1Data,
		RAM2Data => RAM2Data,
		RAM3Data => RAM3Data,
		RAM4Data => RAM4Data,
		IQSEl1 => iqSel1Aux,
		IQSEl2 => IQSel2,
		IQSEl3 => IQSel3,
		IQSEl4 => IQSel4,
		RAM1CS => ram1CSAux,
		RAM1OE => ram1OEAux,
		RAM1WE => ram1WEAux,
		RAM2CS => RAM2CSN,
		RAM2OE => RAM2OEN,
		RAM2WE => RAM2WEN,
		RAM3CS => RAM3CSN,
		RAM3OE => RAM3OEN,
		RAM3WE => RAM3WEN,
		RAM4CS => RAM4CSN,
		RAM4OE => RAM4OEN,
		RAM4WE => RAM4WEN,
		RMReadDiag1 => readDiag1RAM,
		RMReadDiag2 => readDiag2RAM,
		RMReadDiag3 => readDiag3RAM,
		RMReadDiag4 => readDiag4RAM,
		DiagRequest => doAcquisition,
		WriteDiag1Select => writeRegs(DIAG1SELECTR),
		WriteDiag2Select => writeRegs(DIAG2SELECTR),
		WriteDiag3Select => writeRegs(DIAG3SELECTR),
		WriteDiag4Select => writeRegs(DIAG4SELECTR),
		DataFromDecoder => dataFromDecoder,
		Diag1Select => registers(DIAG1SELECTR),
		Diag2Select => registers(DIAG2SELECTR),
		Diag3Select => registers(DIAG3SELECTR),
		Diag4Select => registers(DIAG4SELECTR)
	);
-- temp stuff
TestOut <= "000000" & resCSNAux & "00000000000";

U0_commands: commands PORT MAP(
		Clk40 => clk40,
		ResetNA => ResetNA,
      IQAlarm => iqAlarm,
		CavOvr => CavOvrC,
		FwdOvr => FwdOvrC,
		RefOvr => RefOvrC,
		RFOnFalling => rfOnFalling,
		RFOnRising => rfOnRising,
		FProtect => not XProtectInNA,
		WriteCONTROLR => writeRegs(CONTROLR),
		WriteSWITCHCTRLR => writeRegs(SWITCHCTRLR),
		WriteSOFTSWITCHR => writeRegs(SOFTSWITCHR),
		WriteSTATER => writeRegs(STATER),
		WriteRFONMAXLENGTHR => writeRegs(RFONMAXLENGTHR),
		ReadStatus => readRegs(STATUSR),
		TimeTracker => timeTracker,
		DataFromDecoder => dataFromDecoder,
		CavSwitch => cavSwitch,
		RefSwitch => refSwitch,
		FwdSwitch => fwdSwitch,
		OutSwitch => outSwitch,
		MainLoopSwitch => mainLoopSwitch,
      SatDisabled => satDisabled,
		DoAcquisition => doAcquisition,
		ConfigMode => configMode,
		ProdLocalMode => prodLocalMode,
	   ProdRemoteMode => prodRemoteMode,
		CWMode => cwMode,
		CONTROLR => registers(CONTROLR),
		STATUSR => registers(STATUSR),
		STATER => registers(STATER),
		RFONMAXLENGTHR => registers(RFONMAXLENGTHR),
		SWITCHCTRLR => registers(SWITCHCTRLR),
		SOFTSWITCHR => registers(SOFTSWITCHR)
	);

U0_irqcontrol: irqcontrol PORT MAP(
		Clk => clk40,
		IrqEnable => not configMode,
		RFOn => RfOnA,
		StartCycleA => StartCycleA,
		StartCycle => startCycleRising,
		Reset => resetA,
		WriteVMEIRQR => writeRegs(VMEIRQR),
		ReadIRQSRCR => readRegs(IRQSRCR),
		DataFromDecoder => dataFromDecoder,
		VMEIRQR => registers(VMEIRQR),
		IRQSRCR => registers(IRQSRCR),
		IntProcessed => intProcessed,
		RFOnRising => rfOnRising,
		RFOnFalling => rfOnFalling,
      StateOut => debounceState,
		IrqN => irqToVMEN
	);

Ifeedforward: feedforward PORT MAP(
		Clk40 => clk40,
		SyncReset => SyncReset,
		DataIn => cavIC,
		DataOut => cavIFF,
		RFONFalling => rfOnFalling,
		RFONRising => rfOnRising,
		MainLoopSwitch => mainLoopSwitch,
		FFData => ffIDataToLoop(13 downto 0),
		FFDataValid => ffDataToLoopValid
	);

Qfeedforward: feedforward PORT MAP(
		Clk40 => clk40,
		SyncReset => SyncReset,
		DataIn => cavQC,
		DataOut => cavQFF,
		RFONFalling => rfOnFalling,
		RFONRising => rfOnRising,
		MainLoopSwitch => mainLoopSwitch,
		FFData => ffQDataToLoop(13 downto 0),
		FFDataValid => ffDataToLoopValid
	);


U0_snapshots: snapshots PORT MAP(
		SyncReset => syncReset,
		Clk40 => clk40,
		DataFromDecoder => dataFromDecoder,
		WriteDiagTime => writeRegs(DIAGTIMER),
		DiagTime => registers(DIAGTIMER),
		RFOnRising => rfOnRising,
		RFOnFalling => rfOnFalling,
		ConfigMode => configMode,
		TimeTracker => timeTracker,
		RFOffTime => registers(RFOFFTIMER),
		RefIIn => refIR,
		RefIOut => registers(REFDIAGIR),
		RefQIn => refQR,
		RefQOut => registers(REFDIAGQR),
		FwdIIn => fwdIR,
		FwdIOut => registers(FWDDIAGIR),
		FwdQIn => fwdQR,
		FwdQOut => registers(FWDDIAGQR),
		CavIIn => cavIR,
		CavIOut => registers(CAVDIAGIR),
		CavQIn => cavQR,
		CavQOut => registers(CAVDIAGQR),
		ErrIIn => cavIErr,
		ErrIOut => registers(ERRDIAGIR),
		ErrQIn => cavQErr,
		ErrQOut => registers(ERRDIAGQR),
		IntIIn => cavIInt,
		IntIOut => registers(IOUTDIAGIR),
		IntQIn => cavQInt,
		IntQOut => registers(IOUTDIAGQR),
		PropIIn => cavIProp,
		PropIOut => registers(POUTDIAGIR),
		PropQIn => cavQProp,
		PropQOut => registers(POUTDIAGQR),
		OutIIn => cavIToMod,
		OutIOut => registers(OUTDIAGIR),
		OutQIn => cavQToMod,
		OutQOut => registers(OUTDIAGQR)
	);

U0_iqcounter: iqcounter PORT MAP(
		Clk20 => clk20,
		Clk80 => clk80N,
      Alarm => iqAlarm,
		IQCount => iqCount
	);

RefIQDemod: iqdemodcw PORT MAP(
		DataInA => RefIn,
		Clk80In => clk80N,
		IQCount => iqCount,
		IData => refI,
		QData => refQ
	);

FwdIQDemod: iqdemodcw PORT MAP(
		DataInA => FwdIn,
		Clk80In => clk80N,
      IQCount => iqCount,
		IData => fwdI,
		QData => fwdQ
	);

CavIQDemod: iqdemodcw PORT MAP(
		DataInA => CavIn,
		Clk80In => clk80N,
		IQCount => iqCount,
		IData => cavI,
		QData => cavQ
	);

Ref_mul2x2: mul2x2 PORT MAP(
		IDataIn => refI,
		QDataIn => refQ,
		WriteIA => writeRegs(REFMATRIXAR),
		WriteIB => writeRegs(REFMATRIXCR),
		WriteQA => writeRegs(REFMATRIXBR),
		WriteQB => writeRegs(REFMATRIXDR),
		IFactorA => registers(REFMATRIXAR),
		IFactorB => registers(REFMATRIXCR),
		QFactorA => registers(REFMATRIXBR),
		QFactorB => registers(REFMATRIXDR),
		DataFromDecoder => dataFromDecoder,
		Clk40 => clk40,
		Reset => syncReset,
		IDataOut => refIR,
		QDataOut => refQR
	);

Fwd_mul2x2: mul2x2 PORT MAP(
		IDataIn => fwdI,
		QDataIn => fwdQ,
		WriteIA => writeRegs(FWDMATRIXAR),
		WriteIB => writeRegs(FWDMATRIXCR),
		WriteQA => writeRegs(FWDMATRIXBR),
		WriteQB => writeRegs(FWDMATRIXDR),
		IFactorA => registers(FWDMATRIXAR),
		IFactorB => registers(FWDMATRIXCR),
		QFactorA => registers(FWDMATRIXBR),
		QFactorB => registers(FWDMATRIXDR),
		DataFromDecoder => dataFromDecoder,
		Clk40 => clk40,
		Reset => syncReset,
		IDataOut => fwdIR,
		QDataOut => fwdQR
	);

Cav_mul2x2: mul2x2 PORT MAP(
		IDataIn => cavI,
		QDataIn => cavQ,
		WriteIA => writeRegs(CAVMATRIXAR),
		WriteIB => writeRegs(CAVMATRIXCR),
		WriteQA => writeRegs(CAVMATRIXBR),
		WriteQB => writeRegs(CAVMATRIXDR),
		IFactorA => registers(CAVMATRIXAR),
		IFactorB => registers(CAVMATRIXCR),
		QFactorA => registers(CAVMATRIXBR),
		QFactorB => registers(CAVMATRIXDR),
		DataFromDecoder => dataFromDecoder,
		Clk40 => clk40,
		Reset => syncReset,
		IDataOut => cavIR,
		QDataOut => cavQR
	);

I_picontroller: picontrollercw PORT MAP(
		SyncReset => syncReset,
		Clk40 => clk40,
		DataFromDecoder => dataFromDecoder,
		WriteKP => writeRegs(KPR),
		WriteKI => writeRegs(KIR),
		RFONRising => rfOnRising,
		RFONFalling => rfOnFalling,
      CWMode => cwMode,
      AntiWindUp => antiWindUp,
		ConfigMode => configMode,
		DataIn => cavIR,
		SetPoints => spIDataToPI(13 downto 0),
		SetPointsValid => spDataToPIValid,
		DataOut => cavIC,
      DataOutSat => cavISat,
		KP => registers(KPR),
		KI => registers(KIR),
		PropOut => cavIProp,
		IntOut => cavIInt,
      LongIntOut => cavILongInt,
		ErrorOut => cavIErr
	);

Q_picontroller: picontrollercw PORT MAP(
		SyncReset => syncReset,
		Clk40 => clk40,
		DataFromDecoder => dataFromDecoder,
		WriteKP => writeRegs(KPR),
		WriteKI => writeRegs(KIR),
		RFONRising => rfOnRising,
		RFONFalling => rfOnFalling,
      CWMode => cwMode,
      AntiWindUp => antiWindUp,
		ConfigMode => configMode,
		DataIn => cavQR,
		SetPoints => spQDataToPI(13 downto 0),
		SetPointsValid => spDataToPIValid,
		DataOut => cavQC,
      DataOutSat => cavQSat,
		KP => open,
		KI => open,
		PropOut => cavQProp,
		IntOut => cavQInt,
      LongIntOut => cavQLongInt,
		ErrorOut => cavQErr
	);

U0_satcontrol: satcontrolcw PORT MAP(
		Clk => clk40,
		IDataIn => cavIC,
		QDataIn => cavQC,
		ISatIn => cavISat,
		QSatIn => cavQSat,
		Saturated => saturated,
		Iout => cavIFixed,
		Qout => cavQFixed
	);

cavIToMod <= cavIFF when (saturated='0' or mainLoopSwitch='1' or 
                          (saturated='1' and satDisabled='1')) else cavIFixed;
cavQToMod <= cavQFF when (saturated='0' or mainLoopSwitch='1' or
                         (saturated='1' and satDisabled='1')) else cavQFixed;

U0_iqmodulator: iqmodulator PORT MAP(
		resetna => ResetNA,
		clk => clk80,
		ConfigMode => configMode,
		i => cavIToMod, 
		q => cavQToMod, 
      IQCount => iqCount,
		waveout1 => RFDAC1,
		waveout2 => RFDAC2
	);

U0_rescontrol: rescontrol PORT MAP(
		Reset => syncReset,
		Clk40 => clk40,
		WriteResCtrl => writeRegs(RESCTRLR),
		WriteResCtrlTime => writeRegs(RESCTRLTIMER),
		ResCtrlTime => registers(RESCTRLTIMER),
		CavI => cavIR,
		CavQ => cavQR,
		FwdI => fwdIR,
		FwdQ => fwdQR,	
      RFOnRising => rfOnRising,
      ResFwdI => registers(RESFWDIR),
      ResFwdQ => registers(RESFWDQR),
      ResCavI => registers(RESCAVIR),
      ResCavQ => registers(RESCAVQR),
		DataFromDecoder => dataFromDecoder,
		TimeTracker => timeTracker,
		RESCTRL => registers(RESCTRLR),
		CSN => resCSNAux,
		ResCtrlD => ResCtrlD,
      ResSClk => ResSClk
	);
	
U0_TimingManager: TimingManager PORT MAP (
      SyncReset => syncReset,
      Clk => clk40,
      StartCycle => startCycleRising,
      RFOnRising => rfOnRising,
      WriteNCycle => writeRegs(NEXTCYCLER),
		DataFromDecoder => dataFromDecoder,
      NextCycle => registers(NEXTCYCLER),
      PresentCycle => registers(PRESENTCYCLER),
      PulseNumber => registers(PULSENUMBERR)
		);

OvrClocking: process(clk40)
begin
 if clk40'event and clk40='1' then
  cavOvrC <= CavOvr;
  fwdOvrC <= FwdOvr;
  refOvrC <= RefOvr;
 end if;
end process OvrClocking;

U0_oneshot: oneshot PORT MAP(
		Clk => clk40,
		ResetNA => ResetNA,
		PulseIn => CavOvrC,
		PulseOut => Led3
	);

U1_oneshot: oneshot PORT MAP(
		Clk => clk40,
		ResetNA => ResetNA,
		PulseIn => saturated,
		PulseOut => Led2
	);

U2_oneshot: oneshot PORT MAP(
		Clk => clk40,
		ResetNA => ResetNA,
		PulseIn => badlySaturated,
		PulseOut => Led4
	);

badlySaturated <= '1' when (cavILongInt="011111111111111111111111111" or 
                            cavILongInt="100000000000000000000000000" or 
                            cavQLongInt="011111111111111111111111111" or 
                            cavQLongInt="100000000000000000000000000") else '0';

SwControl <= registers(SWITCHCTRLR)(2) & registers(SWITCHCTRLR)(1) & 
             registers(SWITCHCTRLR)(0) & registers(SWITCHCTRLR)(3);
XProtectOutN <= '1';
XMuxCtrl <= '0';

RAM6Addr <= "000000000000000000";
RAM6Data <= (others=>'0');
RAM6CSN   <= '1';
RAM6OEN   <= '1';
RAM6WEN   <= '1';

RAM5Addr <= "000000000000000000";
RAM5Data <= (others=>'0');
RAM5CSN   <= '1';
RAM5OEN   <= '1';
RAM5WEN   <= '1';

Led5       <= not XProtectInNA;
Led1       <= not configMode;

-- No sleeps
RAMZZ <= (others=>'0');
DIAGZZ <= (others=>'0');

end RTL;
