library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity TimingManager is
    Port ( SyncReset : in  STD_LOGIC;
           Clk : in  STD_LOGIC;
           StartCycle : in  STD_LOGIC;
           RFOnRising : in  STD_LOGIC;
           WriteNCycle : in  STD_LOGIC;
			  DataFromDecoder: in std_logic_vector(15 downto 0);
           NextCycle : out  STD_LOGIC_VECTOR (15 downto 0);
           PresentCycle : out  STD_LOGIC_VECTOR (15 downto 0);
           PulseNumber : out  STD_LOGIC_VECTOR (15 downto 0));
end TimingManager;

architecture RTL of TimingManager is

signal nextCycleAux, presentCycleAux: std_logic_vector(15 downto 0);
signal pulseCounter: unsigned(7 downto 0);

begin

NextCycle <= "00000000000" & nextCycleAux(4 downto 0);
PresentCycle <= presentCycleAux;
PulseNumber <= "00000000" & std_logic_vector(pulseCounter);

RegProcess: process(Clk)
begin
 if Clk'event and Clk='1' then
  if SyncReset='1' then
   nextCycleAux <= X"0000";
  elsif WriteNCycle='1' then
   nextCycleAux <= DataFromDecoder;
  end if;
  if StartCycle='1' then
   presentCycleAux <=  "00000000000" & nextCycleAux(4 downto 0);
  end if;
  if StartCycle='1' then 
   pulseCounter <= (others=>'0');
  elsif RFOnRising='1' then
   pulseCounter <= pulseCounter + 1;
  end if;
 end if;
end process RegProcess;

end RTL;

