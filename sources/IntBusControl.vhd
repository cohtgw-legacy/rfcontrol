library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.IntContPackage.ALL;

entity IntBusControl is
    Port ( AddrFromVME : in std_logic_vector(18 downto 0);
           WriteFromVME : in std_logic;
           ReadFromVME : in std_logic;
           Registers : in RegsType;
           DataToVME : out IntDataType;
           DataToVMEValid : out std_logic;
           RegEnables : out std_logic_vector(NUMREGS-1 downto 0));
end IntBusControl;

architecture RTL of IntBusControl is

signal vAdd: unsigned(18 downto 0);

begin

 vAdd <= unsigned(AddrFromVME);
 dataToVMEValid <= ReadFromVME;

 EnableGeneration: process(vAdd, WriteFromVME)
 variable intAddress: integer;
 begin
  intAddress := to_integer(vAdd);
  if (vAdd(18)='1') then
    RegEnables <= (NUMREGS-1 => '1', others=>'0');
    DataToVME <= (others=>'0');
  elsif (intAddress < NUMREGS-1) then
    case intAddress is
      when 0  => RegEnables <= (0=>WriteFromVME,  others=>'0');
                 DataToVME <= Registers(0);
      when 1  => RegEnables <= (1=>WriteFromVME,  others=>'0');
                 DataToVME <= Registers(1);
      when 2  => RegEnables <= (2=>WriteFromVME,  others=>'0');
                 DataToVME <= Registers(2);
      when 3  => RegEnables <= (3=>WriteFromVME,  others=>'0');
                 DataToVME <= Registers(3);
      when 4  => RegEnables <= (4=>WriteFromVME,  others=>'0');
                 DataToVME <= Registers(4);
      when 5  => RegEnables <= (5=>WriteFromVME,  others=>'0');
                 DataToVME <= Registers(5);
      when 6  => RegEnables <= (6=>WriteFromVME,  others=>'0');
                 DataToVME <= Registers(6);
      when 7  => RegEnables <= (7=>WriteFromVME,  others=>'0');
                 DataToVME <= Registers(7);
      when 8  => RegEnables <= (8=>WriteFromVME,  others=>'0');
                 DataToVME <= Registers(8);
      when 9  => RegEnables <= (9=>WriteFromVME,  others=>'0');
                 DataToVME <= Registers(9);
      when 10 => RegEnables <= (10=>WriteFromVME, others=>'0');
                 DataToVME <= Registers(10);
      when 11 => RegEnables <= (11=>WriteFromVME, others=>'0');
                 DataToVME <= Registers(11);
      when 12 => RegEnables <= (12=>WriteFromVME, others=>'0');
                 DataToVME <= Registers(12);
      when 13 => RegEnables <= (13=>WriteFromVME, others=>'0');
                 DataToVME <= Registers(13);
      when 14 => RegEnables <= (14=>WriteFromVME, others=>'0');
                 DataToVME <= Registers(14);
      when 15 => RegEnables <= (15=>WriteFromVME, others=>'0');
                 DataToVME <= Registers(15);
      when 16 => RegEnables <= (16=>WriteFromVME, others=>'0');
                 DataToVME <= Registers(16);
      when 17 => RegEnables <= (17=>WriteFromVME, others=>'0');
                 DataToVME <= Registers(17);
      when 18 => RegEnables <= (18=>WriteFromVME, others=>'0');
                 DataToVME <= Registers(18);
      when 19 => RegEnables <= (19=>WriteFromVME, others=>'0');
                 DataToVME <= Registers(19);
      when 20 => RegEnables <= (20=>WriteFromVME, others=>'0');
                 DataToVME <= Registers(20);
      when 21 => RegEnables <= (21=>WriteFromVME, others=>'0');
                 DataToVME <= Registers(21);
      when others => RegEnables <= (others=>'0');
                     DataToVME <= (others=>'0');
    end case;
  else
    RegEnables <= (others=>'0');
    DataToVME <= (others=>'0');
  end if;
 end process EnableGeneration;

end RTL;
