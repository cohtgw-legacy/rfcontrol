----------------------------------------------------
--  
--  Unit Name :  VMEDecoder
--
-- Description:
--
--
--  Author  :     Javier Serrano
--  Group:     AB/CO 
--
--  Revisions:   
-- 	1.1. (17 November 2003) Initial release
-- 	1.2. (3 June 2008) Clean-up and comments.
--                     
--
--  For any bug or comment, please send an e-mail to Javier.Serrano@cern.ch
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.RegistersMap.all;

entity VMEDecoder is
    Port ( Clk40 : in std_logic;
           ResetNA: in std_logic;
           AddrFromVME : in std_logic_vector(18 downto 0);
           DataFromVME : in std_logic_vector(15 downto 0);
           DataToVME : out std_logic_vector(15 downto 0);
           DataToVMEValid : out std_logic;
           WriteFromVME : in std_logic; -- 1 tick long
           ReadFromVME : in std_logic; -- 1 tick long
           DataFromDecoder : out std_logic_vector(15 downto 0);
           AddrFromDecoder: out std_logic_vector(18 downto 0);
           WriteRegs : out std_logic_vector(NumberOfRegs-1 downto 0);
           ReadRegs: out std_logic_vector(NumberOfRegs-1 downto 0);
           DataFromRAM: in std_logic_vector(15 downto 0);
           DataFromRAMValid: in std_logic;
           WriteRAM: out std_logic;
           ReadRAM: out std_logic;
           Regs : in RegsType);
end VMEDecoder;

architecture RTL of VMEDecoder is

type StateType is (Idle, RegAccess, RAMAccess);

signal currentState, nextState: StateType;
signal regsSelected, ramSelected: std_logic;
signal vmeAddr: unsigned(18 downto 0);
signal regAddr: unsigned(5 downto 0);
signal regsDataToVME, ramDataToVME: std_logic_vector(15 downto 0);
signal regsDataToVMEValid, ramDataToVMEValid: std_logic;
signal vmeAccess: std_logic;

begin

vmeAddr <= unsigned(AddrFromVME); -- for convenience
regAddr <= vmeAddr(5 downto 0);

vmeAccess <= WriteFromVME or ReadFromVME;
regsSelected <= '1' when  ((vmeAddr >= StartRegs) and (vmeAddr <= StopRegs)) else '0';
ramSelected <= '1' when ((vmeAddr >= StartRAM) and (vmeAddr <= StopRAM)) else '0';

States: process(ResetNA, Clk40)
begin
 if ResetNA ='0' then 
  currentState <= Idle;
 elsif Clk40'event and Clk40='1' then
  currentState <= nextState;
 end if;
end process States;

Transitions: process(currentState, ReadFromVME, regsSelected,
                     RAMSelected, regsDataToVMEValid,
                     ramDataToVMEValid)
begin
 case currentState is 
  when Idle =>
   if ReadFromVME='1' and regsSelected='1' then 
    nextState <= RegAccess;
   elsif ReadFromVME='1' and RAMSelected='1' then
    nextState <= RAMAccess;
   else nextState <= Idle;
   end if;
  when RegAccess =>
   if regsDataToVMEValid='1' then 
    nextState <= Idle;
   else nextState <= RegAccess;
   end if;
  when RAMAccess =>
   if ramDataToVMEValid='1' then 
    nextState <= Idle;
   else nextState <= RAMAccess;
   end if;
  when others =>
   nextState <= Idle;
 end case;
end process Transitions;

Outputs: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  case currentState is
   when Idle =>
     DataToVME <= (others=>'0');
     DataToVMEValid <= '0';
   when RegAccess =>
     DataToVME <= regsDataToVME;
     DataToVMEValid <= regsDataToVMEValid;
   when RAMAccess =>
     DataToVME <= ramDataToVME;
     DataToVMEValid <= ramDataToVMEValid;
   when others =>
     DataToVME <= (others=>'0');
     DataToVMEValid <= '0';
  end case;
 end if;
end process Outputs;

ClockData: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  if WriteFromVME='1' then
   DataFromDecoder <= DataFromVME;
  end if;
 end if;
end process ClockData;

ClockAddress: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  if vmeAccess='1' then
   AddrFromDecoder <= AddrFromVME;
  end if;
 end if;
end process ClockAddress;

ReadWriteRegs: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  if regsSelected='1' then
   for I in 0 to NumberOfRegs-1 loop
    if regAddr=to_unsigned(I, regAddr'length) then
     if ReadFromVME='1' then
      ReadRegs(I) <= '1';
      WriteRegs(I) <= '0';
      regsDataToVME <= Regs(I);
      regsDataToVMEValid <= '1';
     elsif WriteFromVME='1' then 
      ReadRegs(I) <= '0'; 
      WriteRegs(I)<= '1';
     else
      ReadRegs(I) <= '0';
      WriteRegs(I) <= '0';
     end if; 
    else 
     ReadRegs(I) <= '0';
     WriteRegs(I) <= '0';
    end if;
   end loop;
  else 
   ReadRegs <= (others=>'0');
   WriteRegs <= (others=>'0');
  end if;
 end if; 
end process ReadWriteRegs;

ReadWriteRAM: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  if ramSelected='1' and WriteFromVME='1' then
     WriteRAM <= '1';
     ReadRAM  <= '0';
  elsif ramSelected='1' and ReadFromVME='1' then
     WriteRAM <= '0';
     ReadRAM  <= '1';
  else 
     WriteRAM <= '0';
     ReadRAM  <= '0';
  end if;
 end if;
end process ReadWriteRAM;

ramDataToVME <= DataFromRAM;
ramDataToVMEValid <= DataFromRAMValid;

end RTL;
