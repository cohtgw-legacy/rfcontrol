----------------------------------------------------
--  
--  Unit Name :  Commands
--
-- Description:
--
-- Implements commands support.
--
--  Author  :  Javier Serrano
--  Group   :  AB/CO 
--
--  Revisions:   
-- 	1.1. (17 November 2003) Initial release
-- 	1.7. (4 June 2008) Clean-up and comments.
-- TO DO: status register                 
--
--  For any bug or comment, please send an e-mail to Javier.Serrano@cern.ch

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Commands is
    Port ( Clk40 : in std_logic;
           ResetNA : in std_logic;
			  IQAlarm: in std_logic;
			  CavOvr: in std_logic;
			  FwdOvr: in std_logic;
			  RefOvr: in std_logic;
			  RFOnRising: in std_logic;
           RFOnFalling: in std_logic;
           FProtect: in std_logic;
           WriteCONTROLR : in std_logic;
           WriteSWITCHCTRLR : in std_logic;
           WriteSOFTSWITCHR: in std_logic;
			  WriteSTATER: in std_logic;
			  WriteRFONMAXLENGTHR: in std_logic;
			  ReadStatus: in std_logic;
			  TimeTracker: in std_logic_vector(15 downto 0);
           DataFromDecoder : in std_logic_vector(15 downto 0);
           CavSwitch : out std_logic;
           RefSwitch : out std_logic;
           FwdSwitch : out std_logic;
           OutSwitch : out std_logic;
           MainLoopSwitch : out std_logic;
           SatDisabled: out std_logic;
           DoAcquisition: out std_logic;    -- Check the state of this signal at RFOnRising time to decide whether to make acquisition or not.
           ConfigMode: out std_logic;
			  ProdLocalMode: out std_logic;
			  ProdRemoteMode: out std_logic;
			  CWMode: out std_logic;
           CONTROLR : out std_logic_vector(15 downto 0);
           STATUSR : out std_logic_vector(15 downto 0);
			  STATER: out std_logic_vector(15 downto 0);
			  RFONMAXLENGTHR: out std_logic_vector(15 downto 0);
           SWITCHCTRLR : out std_logic_vector(15 downto 0);
           SOFTSWITCHR : out std_logic_vector(15 downto 0));
end Commands;

architecture RTL of Commands is

type StateType is (Idle, WaitingRFRising, WaitingRFFalling);
signal currentState, nextState: StateType;
signal switchCtrlAux, softSwitchAux, controlAux, stateAux: std_logic_vector(15 downto 0);
signal configAux, prodLocalAux, prodRemoteAux: std_logic;
signal rfOnToggle, aqnPending, satDisabledAux, cwModeAux: std_logic;
signal maxLengthAux: std_logic_vector(15 downto 0);
signal cavOvrAux, fwdOvrAux, refOvrAux: std_logic;
signal lengthAlarm, iqAlarmAux: std_logic;

begin

RefSwitch <= switchCtrlAux(0);
FwdSwitch <= switchCtrlAux(1);
CavSwitch <= switchCtrlAux(2);
OutSwitch <= switchCtrlAux(3);
MainLoopSwitch <= softSwitchAux(0);
SWITCHCTRLR <= switchCtrlAux;
SOFTSWITCHR <= softSwitchAux;
STATER <= stateAux;
CONTROLR <= controlAux(15 downto 1) & aqnPending;
SatDisabled <= satDisabledAux;
STATUSR <= "0000000000" & iqAlarmAux & FProtect & lengthAlarm & cavOvrAux & fwdOvrAux & refOvrAux;
configAux <= '1' when stateAux(1 downto 0)="00" else '0';
prodLocalAux <= '1' when stateAux(1 downto 0)="01" else '0';
prodRemoteAux <= '1' when stateAux(1 downto 0)="10" else '0';
ConfigMode <= configAux;
ProdLocalMode <= prodLocalAux;
ProdRemoteMode <= prodRemoteAux;
DoAcquisition <= aqnPending and prodRemoteAux;
RFONMAXLENGTHR <= maxLengthAux;


stateTransitions: process(ResetNA, Clk40)
begin
 if ResetNA='0' then
  currentState <= Idle;
 elsif Clk40'event and Clk40='1' then
  currentState <= nextState;
 end if;
end process stateTransitions;

FSM: process(currentState, aqnPending, prodRemoteAux, RFOnRising, RFOnFalling)
begin
   case currentState is
      when Idle =>
         if aqnPending='1' and prodRemoteAux='1' then
           nextState <= WaitingRFRising;
         else nextState <= Idle;
         end if;
      when WaitingRFRising =>
         if RFOnRising='1' then
           nextState <= WaitingRFFalling;
         else nextState <= WaitingRFRising;
         end if;
		when WaitingRFFalling =>
         if RFOnFalling='1' then
           nextState <= Idle;
         else nextState <= WaitingRFFalling;
         end if;
      when others =>
         nextState <= Idle;
   end case; 
end process FSM;


Decoder: process(ResetNA, Clk40)
begin

 if ResetNA='0' then
  switchCtrlAux <= (others=>'0');
  softSwitchAux <= (others=>'0');
  stateAux <= (others=>'0');
  controlAux <= (others=>'0');
  rfOnToggle <= '0';
  aqnPending <= '0';
  satDisabledAux <= '0';
  cwModeAux <= '0';
  cavOvrAux <= '0';
  fwdOvrAux <= '0';
  refOvrAux <= '0';
  lengthAlarm <= '0';
  iqAlarmAux <= '0';
 elsif Clk40'event and Clk40='1' then

  if RfOnFalling='1' then
   rfOnToggle <= not rfOnToggle;
  end if;

  if WriteCONTROLR='1' then 
   controlAux <= DataFromDecoder;
   aqnPending <= DataFromDecoder(0);
	cwModeAux <= DataFromDecoder(1);
	satDisabledAux <= DataFromDecoder(3);
  elsif currentState=WaitingRFFalling and nextState=Idle then
   aqnPending <= '0';
  end if;

  if WriteSWITCHCTRLR='1' then
   switchCtrlAux <= DataFromDecoder;
  end if;
  
  if WriteRFONMAXLENGTHR='1' then
   maxLengthAux <= DataFromDecoder;
  end if;
  
  if WriteSTATER='1' then
   stateAux <= DataFromDecoder;
  end if;
  
  if WriteSOFTSWITCHR='1' then
   softSwitchAux <= DataFromDecoder;
  end if;
  
  if cavOvr='1' then
   cavOvrAux <= '1';
  elsif ReadStatus='1' then
   cavOvrAux <= '0';
  end if;

  if fwdOvr='1' then
   fwdOvrAux <= '1';
  elsif ReadStatus='1' then
   fwdOvrAux <= '0';
  end if;

  if refOvr='1' then
   refOvrAux <= '1';
  elsif ReadStatus='1' then
   refOvrAux <= '0';
  end if;
  
  if RFOnFalling='1' then 
   if unsigned(maxLengthAux) < unsigned(TimeTracker) then
	 lengthAlarm <= '1';
	elsif ReadStatus='1' then
	 lengthAlarm <= '0';
	end if;
  elsif ReadStatus='1' then
   lengthAlarm <= '0';
  end if;
  
  if IQAlarm='1' then
   iqAlarmAux <= '1';
  elsif ReadStatus='1' then
   iqAlarmAux <= '0';
  end if;
  
 end if; -- clk event
end process Decoder;



end RTL;
