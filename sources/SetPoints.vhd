library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity SetPoints is
    Port ( SyncReset : in std_logic;
           Clk40 : in std_logic;

           AddrFromRM : in std_logic_vector(17 downto 0);
           DataFromRM : in std_logic_vector(15 downto 0);
           ReadFromRM : in std_logic;
           WriteFromRM : in std_logic;
           DataToRM : out std_logic_vector(15 downto 0);
           DataToRMValid : out std_logic;

           RFONFalling: in std_logic;
           RFONRising: in std_logic;
			  PresentCycle: in std_logic_vector(4 downto 0);
           ConfigMode: in std_logic;

           IDataToProcess: out std_logic_vector(15 downto 0);
           QDataToProcess: out std_logic_vector(15 downto 0);
           DataToProcessValid: out std_logic;

           RAMAddress : out std_logic_vector(17 downto 0);
           RAMData : inout std_logic_vector(15 downto 0);
           RAMOE : out std_logic;
           RAMCS : out std_logic;
           RAMWE : out std_logic);
end SetPoints;

architecture RTL of SetPoints is

type StateType is (Idle, rmRead, rmReadWaiting, rmReadWaiting2, rmReadWaiting3, rmDataValid, rmWrite, rmWriteDone, rfOnRead);

signal currentState, nextState: StateType;
signal ramDataIn, ramDataInC, ramDataOut: std_logic_vector(15 downto 0);
signal writing: std_logic;
signal rmPendingAddr: std_logic_vector(17 downto 0);
signal rmPendingData: std_logic_vector(15 downto 0);
signal rmPendingWrite, rmPendingRead: std_logic;
signal ramAddrAux: unsigned(17 downto 0);
signal spCounter: unsigned(4 downto 0);
signal iAuxH, iAuxL, qAuxH, qAuxL: std_logic_vector(15 downto 0);
signal iAuxn, qAuxn: signed(29 downto 0); 
signal iCurrent, qCurrent: signed(29 downto 0);
signal bigStepI, bigStepQ: signed(29 downto 0);
signal smallStepI, smallStepQ: signed(29 downto 0);
signal incrementI, incrementQ: signed(29 downto 0);
signal iOutAux, qOutAux: signed(29 downto 0);
signal presCycleAux: std_logic_vector(4 downto 0);
signal xn, xnAux: unsigned(15 downto 0);
signal nextAddr: std_logic_vector(12 downto 0);

begin

RAMData <= ramDataOut when writing='1' else (others=>'Z');
ramDataIn <= RAMData;
-- RAMCS <= '0';
RAMAddress <= std_logic_vector(ramAddrAux); 

smallStepI <= bigStepI(29) & bigStepI(29) & bigStepI(29) & 
              bigStepI(29) & bigStepI(29) & bigStepI(29 downto 5);
smallStepQ <= bigStepQ(29) & bigStepQ(29) & bigStepQ(29) & 
              bigStepQ(29) & bigStepQ(29) & bigStepQ(29 downto 5);

incProcess: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  if spCounter=to_unsigned(31, spCounter'length) then
   incrementI <= (others=>'0');
   incrementQ <= (others=>'0');
  else 
   incrementI <= incrementI + smallStepI;
   incrementQ <= incrementQ + smallStepQ;
  end if;  
 end if;
end process incProcess;

IDataToProcess <= std_logic_vector(iOutAux(29) & iOutAux(29) & iOutAux(29 downto 16));
QDataToProcess <= std_logic_vector(qOutAux(29) & qOutAux(29) & qOutAux(29 downto 16));


PendingRM: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  if SyncReset = '1' then
   rmPendingWrite <= '0';
   rmPendingRead <= '0';
  elsif ReadFromRM='1' then
   rmPendingAddr <= AddrFromRM;
   rmPendingRead <= '1';
  elsif WriteFromRM='1' then
   rmPendingAddr <= AddrFromRM;
   rmPendingData <= DataFromRM;
   rmPendingWrite <= '1';
  elsif currentState=rmDataValid or currentState=rmWrite then
   rmPendingWrite <= '0';
   rmPendingRead <= '0';
  end if;  
 end if;
end process PendingRM;

clockCycle: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  if SyncReset = '1' then
   presCycleAux <= (others=>'0');
  else 
   presCycleAux <= PresentCycle;
  end if;  
 end if;
end process clockCycle;

CounterProcess: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  if RFONRising = '1' then
   spCounter <= (others=>'0');
  else
   spCounter <= spCounter + 1;
  end if;  
 end if;
end process CounterProcess;


States: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  if SyncReset='1' then 
   currentState <= Idle;
  else currentState <= nextState;
  end if;
 end if; 
end process States;

Transitions: process(currentState, rmPendingRead, rmPendingWrite, ConfigMode, rfOnRising, rfOnFalling)
begin
 case currentState is 
  when Idle =>
   if rmPendingRead='1' and ConfigMode='1' then
    nextState <= rmRead;
   elsif rmPendingWrite='1' and ConfigMode='1' then
    nextState <= rmWrite;
   elsif configMode='0' and RFONRising='1' then
    nextState <= rfOnRead;
   else 
    nextState <= Idle;
   end if;
  when rmRead =>
   nextState <= rmReadWaiting;
  when rmReadWaiting =>
   nextState <= rmReadWaiting2; 
  when rmReadWaiting2 =>
   nextState <= rmReadWaiting3; 
  when rmReadWaiting3 =>
   nextState <= rmDataValid; 
  when rmDataValid =>
   nextState <= Idle;
  when rmWrite =>
   nextState <= rmWriteDone; 
  when rmWriteDone =>
   nextState <= Idle; 
  when rfOnRead =>
   if RFONFalling='1' then
    nextState <= Idle;
   else 
    nextState <= rfOnRead;
   end if;
  when others =>
   nextState <= Idle;
 end case;
end process Transitions;

Outputs: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  case currentState is
   when Idle =>
    writing <= '0';
	 iAuxn <= (others=>'0');
	 qAuxn <= (others=>'0');
	 iCurrent <= (others=>'0');
	 qCurrent <= (others=>'0');
	 bigStepI <= (others=>'0');
	 bigStepQ <= (others=>'0');
	 iOutAux <= (others=>'0');
	 qOutAux <= (others=>'0');
	 xn <= (others=>'0');
	 xnAux <= (others=>'1');
	 iAuxH <= (others=>'0');
	 iAuxL <= (others=>'0');
	 qAuxH <= (others=>'0');
	 qAuxL <= (others=>'0');
	 nextAddr <= (others=>'0');
    if configMode='0' and RFONRising='1' then
     RAMOE <= '0';
	  RAMCS <= '0';
    else
     RAMOE <= '1';
	  RAMCS <= '1';
    end if;
    RAMWE <= '1';
    DataToRMValid <= '0';
    ramAddrAux <= (others=>'0');
    DataToProcessValid <= '0';
   when rmRead =>
    ramAddrAux <= unsigned(rmPendingAddr);
    RAMOE <= '0'; 
	 RAMCS <= '0';
   when rmReadWaiting =>
    RAMOE <= '0'; 
   when rmReadWaiting2 =>
    RAMOE <= '0'; 
   when rmDataValid =>
    DataToRM <= ramDataInC;
    DataToRMValid <= '1';
   when rmWrite =>
    ramAddrAux <= unsigned(rmPendingAddr);
    ramDataOut <= rmPendingData;
    writing <= '1';
    RAMWE <= '0';
	 RAMCS <= '0';
   when rmWriteDone =>
    RAMWE <= '1';
	 RAMCS <= '1';
   when rfOnRead =>
	 DataToProcessValid <= '1';
	 iOutAux <= iCurrent + incrementI;
	 qOutAux <= qCurrent + incrementQ;
    if xn=to_unsigned(0, xn'length) then
	  if  
	       (spCounter=to_unsigned(4, spCounter'length) or
	        spCounter=to_unsigned(8, spCounter'length) or
		     spCounter=to_unsigned(12, spCounter'length) or
			  spCounter=to_unsigned(16, spCounter'length) or
			  spCounter=to_unsigned(22, spCounter'length)) then
      ramAddrAux <= (unsigned(presCycleAux)) & (resize(unsigned(ramAddrAux + 1),13));
	  elsif spCounter=to_unsigned(26, spCounter'length) then
      ramAddrAux <= unsigned(presCycleAux & nextAddr);
	  end if;
	 end if;
 
	 if xn=to_unsigned(0, xn'length) and spCounter=to_unsigned(6, spCounter'length) then
     nextAddr <= ramDataInC(12 downto 0);
    end if; 
    if xn=to_unsigned(0, xn'length) and spCounter=to_unsigned(10, spCounter'length) then
     xnAux <= unsigned(ramDataInC);
    end if;
	 if xn=to_unsigned(0, xn'length) and spCounter=to_unsigned(14, spCounter'length) then
     iAuxH <= ramDataInC;
    end if;
	 if xn=to_unsigned(0, xn'length) and spCounter=to_unsigned(18, spCounter'length) then
     iAuxL <= ramDataInC;
    end if;
	 if xn=to_unsigned(0, xn'length) and spCounter=to_unsigned(24, spCounter'length) then
     qAuxH <= ramDataInC;
    end if;
	 if xn=to_unsigned(0, xn'length) and spCounter=to_unsigned(28, spCounter'length) then
     qAuxL <= ramDataInC;
    end if;
	 
	 if spCounter=to_unsigned(31, spCounter'length) then
	  if xn=to_unsigned(0, xn'length) then
	   xn <= xnAux - 1;
	  else
	   xn <= xn - 1;
	  end if;
	  iAuxn <= signed(iAuxH(13 downto 0) & iAuxL);
     qAuxn <= signed(qAuxH(13 downto 0) & qAuxL);
	  iCurrent <= iCurrent + iAuxn;
	  qCurrent <= qCurrent + qAuxn;
	  bigStepI <= signed(iAuxH(13 downto 0) & iAuxL);
	  bigStepQ <= signed(qAuxH(13 downto 0) & qAuxL);
	 end if;
   when others =>
    writing <= '0';
    RAMOE <= '1';
    RAMWE <= '1';
	 RAMCS <= '1';
    DataToRMValid <= '0';
  end case;
  ramDataInC <= ramDataIn;
 end if;
end process Outputs;

end RTL;
