library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity PIController is
    Port ( SyncReset : in std_logic;
           Clk40 : in std_logic;
           DataFromDecoder : in std_logic_vector(15 downto 0);
           WriteKP : in std_logic;
           WriteKI : in std_logic;
           RFONRising : in std_logic;
           RFONFalling : in std_logic;
           ConfigMode : in std_logic;
           DataIn : in std_logic_vector(13 downto 0);
           SetPoints : in std_logic_vector(13 downto 0);
           SetPointsValid : in std_logic;
           DataOut : out std_logic_vector(13 downto 0);
           DataOutSat: out std_logic_vector(13 downto 0);
           KP : out std_logic_vector(15 downto 0);
           KI : out std_logic_vector(15 downto 0);
           PropOut : out std_logic_vector(13 downto 0);
           IntOut : out std_logic_vector(13 downto 0);
           LongIntOut: out std_logic_vector(17 downto 0);
           ErrorOut : out std_logic_vector(13 downto 0));
end PIController;

architecture RTL of PIController is

signal kpAux, kiAux: std_logic_vector(15 downto 0);
signal kpSigned, kiSigned: signed(13 downto 0);
signal currentSP, dataInSigned: signed(13 downto 0);
signal error15: signed(14 downto 0);
signal kpError, kiError, kiErrorHigh, kiErrorLow: signed(27 downto 0);
signal kiErrorAux: signed(40 downto 0);
signal kiErrorLong: signed(31 downto 0);
signal integral27: signed(26 downto 0);
signal integral14: signed(13 downto 0);
signal integral28: signed(27 downto 0);
signal integral18: signed(17 downto 0);
signal integral19: signed(18 downto 0);
signal integrateOn: std_logic;
signal propOutAux, intOutAux, dataOutAux14, error14, kiErrorSat: signed(13 downto 0);
signal dataOutAux15: signed(14 downto 0);
signal dataSatAux: signed(19 downto 0);
signal intOutLongAux: signed(18 downto 0);
signal intHigh, intLow: signed(13 downto 0);

begin

KP <= kpAux;
KI <= kiAux;

intHigh <= integral27(26 downto 13);
intLow <= '0' & integral27(12 downto 0);
kiErrorAux <= (kiErrorHigh & "0000000000000") + ("0000000000000" & kiErrorLow);
kiError <= kiErrorAux(40 downto 13);

LongIntOut <= std_logic_vector(integral18);

dataInSigned <= signed(DataIn);
error15 <= (resize(currentSP, error15'length) - resize(dataInSigned, error15'length)) 
            when integrateOn='1' else (others=>'0');

-- propOutAux <= kpError(27 downto 14);
intOutAux <= kiErrorSat;
intOutLongAux <= kiErrorLong(31 downto 13);

dataOutAux15 <= resize(intOutAux, dataOutAux15'length) + 
                resize(propOutAux, dataOutAux15'length);
dataSatAux <= resize(intOutLongAux, dataSatAux'length) + 
                resize(propOutAux, dataSatAux'length);
DataOut <= std_logic_vector(dataOutAux14);
integral14 <= integral27(26 downto 13);

PropOutProcess: process(kpError)
begin
 if kpError >= to_signed(524287, kpError'length) then
  propOutAux <= to_signed(8191, propOutAux'length);
 elsif kpError <= to_signed(-524288, kpError'length) then
  propOutAux <= to_signed(-8192, propOutAux'length);
 else 
  propOutAux <= kpError(19 downto 6);
 end if;
end process PropOutProcess;

IntOutProcess: process(kiError)
begin
 if kiError >= to_signed(8191, kiError'length) then
  kiErrorSat <= to_signed(8191, kiErrorSat'length);
 elsif kiError <= to_signed(-8192, kiError'length) then
  kiErrorSat <= to_signed(-8192, kiErrorSat'length);
 else 
  kiErrorSat <= kiError(13 downto 0);
 end if;
end process IntOutProcess;


Registers: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  kpSigned <= signed(kpAux(13 downto 0));
  kiSigned <= signed(kiAux(13 downto 0));
  ErrorOut <= std_logic_vector(error14);
  PropOut <= std_logic_vector(propOutAux);
  IntOut <= std_logic_vector(intOutAux);
  if SyncReset='1' then
   kpAux <= "0001111111111111";        -- 8191
   kiAux <= (others=>'0');
  elsif WriteKP='1' then
   kpAux <= DataFromDecoder;
  elsif WriteKI='1' then
   kiAux <= DataFromDecoder;
  end if;
 end if;
end process Registers;

SetPointProcess: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  if SyncReset='1' or integrateOn='0' then
   currentSP <= (others=>'0');
  elsif SetPointsValid='1' then
   currentSP <= signed(SetPoints);
  end if;
 end if;
end process SetPointProcess;

Integrator: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  kiErrorHigh <= kiSigned * intHigh;
  kiErrorLow <= kiSigned * intLow;
--  kiError <= kiSigned * integral14;
  kiErrorLong <= kiSigned * integral18;
  if ConfigMode='0' and RFONRising='1' then
   integrateOn <= '1';
  elsif RFOnFalling='1' then
   integrateOn <= '0';
  end if;
  if SyncReset='1' or RFOnFalling='1' then
   integral28 <= (others=>'0');
   integral19 <= (others=>'0');
  elsif integrateOn='1' then
   integral28 <= resize(integral27, integral28'length) + 
                 resize(error14, integral28'length); 
   integral19 <= resize(integral18, integral19'length) + 
                 resize(error14, integral19'length); 
  end if;
 end if;
end process Integrator;

IntSaturation: process(integral28)
begin
 if integral28 >= to_signed(67108863, integral28'length) then    -- 2^26 - 1
  integral27 <= to_signed(67108863, integral27'length);
 elsif integral28 <= to_signed(-67108864, integral28'length) then    -- -2^26
  integral27 <= to_signed(-67108864, integral27'length);
 else   -- no saturation
  integral27 <= integral28(26 downto 0);
 end if;
end process IntSaturation;

LongIntSaturation: process(integral19)
begin
 if integral19 >= to_signed(131071, integral19'length) then    -- 2^17 - 1
  integral18 <= to_signed(131071, integral18'length);
 elsif integral19 <= to_signed(-131072, integral19'length) then    -- -2^17
  integral18 <= to_signed(-131072, integral18'length);
 else   -- no saturation
  integral18 <= integral19(17 downto 0);
 end if;
end process LongIntSaturation;


--Proportional: process(Clk40)
--begin
-- if Clk40'event and Clk40='1' then
  kpError <= kpSigned * error14;
-- end if;
--end process Proportional;

Sum: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  if dataOutAux15 >= to_signed(8191, dataOutAux15'length) then       -- 2^13 - 1
   dataOutAux14 <= to_signed(8191, dataOutAux14'length);
  elsif dataOutAux15 <= to_signed(-8182, dataOutAux15'length) then   -- -2^13
   dataOutAux14 <= to_signed(-8192, dataOutAux14'length);
  else
   dataOutAux14 <= dataOutAux15(13 downto 0);
  end if;
 end if;
end process Sum;

SatSum: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
   DataOutSat <= std_logic_vector(dataOutAux15(14 downto 1));
 end if;
end process SatSum;

ErrSaturation: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  if error15 >= to_signed(8191, error15'length) then       -- 2^13 - 1
   error14 <= to_signed(8191, error14'length);
  elsif error15 <= to_signed(-8192, error15'length) then   -- -2^13
   error14 <= to_signed(-8192, error14'length);
  else
   error14 <= error15(13 downto 0);
  end if;
 end if;
end process ErrSaturation;

end RTL;
