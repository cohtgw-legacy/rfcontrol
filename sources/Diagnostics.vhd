library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Diagnostics is
    Port ( Clk40 : in std_logic;
           Clk80 : in std_logic;
           SyncReset : in std_logic;
           RFONRising: in std_logic;
           RFONFalling: in std_logic;
			  ProdLocalMode: in std_logic;
			  ProdRemoteMode: in std_logic;
           RefI: in std_logic_vector(13 downto 0);
           RefQ: in std_logic_vector(13 downto 0);
           FwdI: in std_logic_vector(13 downto 0);
           FwdQ: in std_logic_vector(13 downto 0);
           CavI: in std_logic_vector(13 downto 0);
           CavQ: in std_logic_vector(13 downto 0);
           ErrI: in std_logic_vector(13 downto 0);
           ErrQ: in std_logic_vector(13 downto 0);
           OutI: in std_logic_vector(13 downto 0);
           OutQ: in std_logic_vector(13 downto 0);
           AddrFromRM : in std_logic_vector(17 downto 0);
           DataToRM: out std_logic_vector(15 downto 0);
           DataToRMValid: out std_logic;
           RAMAddr: out std_logic_vector(17 downto 0);
           RAM1Data: inout std_logic_vector(15 downto 0);
           RAM2Data: inout std_logic_vector(15 downto 0);
           RAM3Data: inout std_logic_vector(15 downto 0);
           RAM4Data: inout std_logic_vector(15 downto 0);
           IQSEl1: out std_logic;
           IQSEl2: out std_logic;
           IQSEl3: out std_logic;
           IQSEl4: out std_logic;
           RAM1CS: out std_logic;
           RAM1OE: out std_logic;
           RAM1WE: out std_logic;
           RAM2CS: out std_logic;
           RAM2OE: out std_logic;
           RAM2WE: out std_logic;
           RAM3CS: out std_logic;
           RAM3OE: out std_logic;
           RAM3WE: out std_logic;
           RAM4CS: out std_logic;
           RAM4OE: out std_logic;
           RAM4WE: out std_logic;
           RMReadDiag1 : in std_logic;
           RMReadDiag2 : in std_logic;
           RMReadDiag3 : in std_logic;
           RMReadDiag4 : in std_logic;
           DiagRequest: in std_logic;
           WriteDiag1Select : in std_logic;
           WriteDiag2Select : in std_logic;
           WriteDiag3Select : in std_logic;
           WriteDiag4Select : in std_logic;
           DataFromDecoder : in std_logic_vector(15 downto 0);
           Diag1Select : out std_logic_vector(15 downto 0);
           Diag2Select : out std_logic_vector(15 downto 0);
           Diag3Select : out std_logic_vector(15 downto 0);
           Diag4Select : out std_logic_vector(15 downto 0));
end Diagnostics;

architecture RTL of Diagnostics is

type StateType is (Idle, ReadRAM, ReadRAMWaiting, ReadRAMWaiting2, ReadRAMDone, IToDAC, QToDac);

signal currentState, nextState: StateType;
signal diag1SelAux, diag2SelAux, diag3SelAux, diag4SelAux: std_logic_vector(15 downto 0); 
signal diag1I, diag1Q: std_logic_vector(13 downto 0);
signal diag2I, diag2Q: std_logic_vector(13 downto 0);
signal diag3I, diag3Q: std_logic_vector(13 downto 0);
signal diag4I, diag4Q: std_logic_vector(13 downto 0);
signal dataToRAM1, dataToRAM2, dataToRAM3, dataToRAM4: std_logic_vector(15 downto 0);
signal dataToRAM1Aux, dataToRAM2Aux, dataToRAM3Aux, dataToRAM4Aux: std_logic_vector(15 downto 0);
signal dataFromRAM1, dataFromRAM2, dataFromRAM3, dataFromRAM4: std_logic_vector(15 downto 0);
signal iqSelAux: std_logic;
signal writing, readRequest: std_logic;
signal ramWE: std_logic;
signal ramAddrAux: unsigned(17 downto 0);
signal rmPendingAddr: std_logic_vector(17 downto 0);
signal pendingRead1, pendingRead2, pendingRead3, pendingRead4: std_logic;
signal iqSel1Aux, iqSel2Aux, iqSel3Aux, iqSel4Aux: std_logic; 

attribute syn_keep : boolean;
signal and_out, keep1, keep2: bit;
attribute syn_keep of keep1, keep2 : signal is true;
attribute syn_keep of iqSel1Aux, iqSel2Aux, iqSel3Aux, iqSel4Aux: signal is true;


begin

Diag1Select <= diag1SelAux;
Diag2Select <= diag2SelAux;
Diag3Select <= diag3SelAux;
Diag4Select <= diag4SelAux;
iqSel1Aux <= iqSelAux;
iqSel2Aux <= iqSelAux;
iqSel3Aux <= iqSelAux;
iqSel4Aux <= iqSelAux;


process(Clk80)
begin
 if Clk80'event and Clk80='1' then
  IQSEl1 <= iqSel1Aux;
  IQSEl2 <= iqSel2Aux;
  IQSEl3 <= iqSel3Aux;
  IQSEl4 <= iqSel4Aux;
  dataToRAM1 <= dataToRAM1Aux;
  dataToRAM2 <= dataToRAM2Aux;
  dataToRAM3 <= dataToRAM3Aux;
  dataToRAM4 <= dataToRAM4Aux;
 end if;
end process;

RAM1WE <= ramWE;
RAM2WE <= ramWE;
RAM3WE <= ramWE;
RAM4WE <= ramWE;

RAM1CS <= '0';
RAM2CS <= '0';
RAM3CS <= '0';
RAM4CS <= '0';

readRequest <= RMReadDiag1 or RMReadDiag2 or RMReadDiag3 or RMReadDiag4;
RAM1Data <= dataToRAM1 when writing='1' else (others=>'Z');
RAM2Data <= dataToRAM2 when writing='1' else (others=>'Z');
RAM3Data <= dataToRAM3 when writing='1' else (others=>'Z');
RAM4Data <= dataToRAM4 when writing='1' else (others=>'Z');
dataFromRAM1 <= RAM1Data;
dataFromRAM2 <= RAM2Data;
dataFromRAM3 <= RAM3Data;
dataFromRAM4 <= RAM4Data;
RAMAddr <= std_logic_vector(ramAddrAux); 

PendingAccess: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  if readRequest='1' then
   rmPendingAddr <= AddrFromRM;
   pendingRead1 <= RMReadDiag1;
   pendingRead2 <= RMReadDiag2;
   pendingRead3 <= RMReadDiag3;
   pendingRead4 <= RMReadDiag4;
  end if;
 end if;
end process PendingAccess;

States: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  if SyncReset='1' then 
   currentState <= Idle;
  else currentState <= nextState;
  end if;
 end if; 
end process States;

Transitions: process(currentState, readRequest, DiagRequest, RFONFalling, RFONRising,
                     ProdLocalMode, ProdRemoteMode)
begin
 case currentState is 
  when Idle =>
   if readRequest='1' then
    nextState <= ReadRAM;
   elsif (DiagRequest='1' and RFONRising='1' and ProdRemoteMode='1') or
	       (RFONRising='1' and ProdLocalMode='1') then
    nextState <= IToDAC;
   else 
    nextState <= Idle;
   end if;
  when ReadRAM =>
   nextState <= ReadRAMWaiting; 
  when ReadRAMWaiting =>
   nextState <= ReadRAMWaiting2; 
  when ReadRAMWaiting2 =>
   nextState <= ReadRAMDone; 
  when ReadRAMDone =>
   nextState <= Idle;
  when IToDAC =>
   if RFONFalling='1' then
    nextState <= Idle;
   else 
    nextState <= QToDAC; 
   end if;
  when QToDAC =>
   if RFONFalling='1' then
    nextState <= Idle;
   else 
    nextState <= IToDAC;
   end if;
  when others =>
   nextState <= Idle;
 end case;
end process Transitions;

Outputs: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  case currentState is
   when Idle =>
    writing <= '1';
    ramWE <= '1';
    RAM1OE <= '1';
    RAM2OE <= '1';
    RAM3OE <= '1';
    RAM4OE <= '1';
    iqSelAux <= '0';
    DataToRM <= (others=>'0');
    DataToRMValid <= '0';
    ramAddrAux <= (others=>'0');
    dataToRAM1Aux <= X"0200";
    dataToRAM2Aux <= X"0200";
    dataToRAM3Aux <= X"0200";
    dataToRAM4Aux <= X"0200";
   when ReadRAM =>
    writing <= '0';
    ramAddrAux <= unsigned(rmPendingAddr);
    if pendingRead1='1' then
     RAM1OE <= '0';
    elsif pendingRead2='1' then
     RAM2OE <= '0';
    elsif pendingRead3='1' then
     RAM3OE <= '0';
    elsif pendingRead4='1' then
     RAM4OE <= '0';
    end if; 
   when ReadRAMWaiting =>
    if pendingRead1='1' then
     RAM1OE <= '0';
    elsif pendingRead2='1' then
     RAM2OE <= '0';
    elsif pendingRead3='1' then
     RAM3OE <= '0';
    elsif pendingRead4='1' then
     RAM4OE <= '0';
    end if; 
   when ReadRAMWaiting2 =>
    if pendingRead1='1' then
     RAM1OE <= '0';
    elsif pendingRead2='1' then
     RAM2OE <= '0';
    elsif pendingRead3='1' then
     RAM3OE <= '0';
    elsif pendingRead4='1' then
     RAM4OE <= '0';
    end if; 
   when ReadRAMDone =>
    if pendingRead1='1' then
     DataToRM <= dataFromRAM1;
    elsif pendingRead2='1' then
     DataToRM <= dataFromRAM2;
    elsif pendingRead3='1' then
     DataToRM <= dataFromRAM3;
    elsif pendingRead4='1' then
     DataToRM <= dataFromRAM4;
    end if;    
    DataToRMValid <= '1';
   when IToDAC =>
	 writing <= '1';
    iqSelAux <= '0';
    dataToRAM1Aux <= "00" & diag1I(3 downto 0) & not(diag1I(13)) & diag1I(12 downto 4);
    dataToRAM2Aux <= "00" & diag2I(3 downto 0) & not(diag2I(13)) & diag2I(12 downto 4);
    dataToRAM3Aux <= "00" & diag3I(3 downto 0) & not(diag3I(13)) & diag3I(12 downto 4);
    dataToRAM4Aux <= "00" & diag4I(3 downto 0) & not(diag4I(13)) & diag4I(12 downto 4);
    if ramAddrAux /= to_unsigned(0, ramAddrAux'length) then
	  ramAddrAux <= ramAddrAux + 1;
	 end if;
    ramWE <= '0';
   when QToDAC =>
	 writing <= '1';
    iqSelAux <= '1';
    dataToRAM1Aux <= "00" & diag1Q(3 downto 0) & not(diag1Q(13)) & diag1Q(12 downto 4);
    dataToRAM2Aux <= "00" & diag2Q(3 downto 0) & not(diag2Q(13)) & diag2Q(12 downto 4);
    dataToRAM3Aux <= "00" & diag3Q(3 downto 0) & not(diag3Q(13)) & diag3Q(12 downto 4);
    dataToRAM4Aux <= "00" & diag4Q(3 downto 0) & not(diag4Q(13)) & diag4Q(12 downto 4);
    ramAddrAux <= ramAddrAux + 1;
    ramWE <= '0';
   when others =>
    writing <= '0';
    ramWE <= '1';
    RAM1OE <= '1';
    RAM2OE <= '1';
    RAM3OE <= '1';
    RAM4OE <= '1';
    iqSelAux <= '0';
    DataToRM <= (others=>'0');
    DataToRMValid <= '0';
    ramAddrAux <= (others=>'0');
    dataToRAM1Aux <= (others=>'0');
    dataToRAM2Aux <= (others=>'0');
    dataToRAM3Aux <= (others=>'0');
    dataToRAM4Aux <= (others=>'0');
  end case;
 end if;
end process Outputs;

Registers: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  if SyncReset='1' then
   diag1SelAux <= (others=>'0');
   diag2SelAux <= (others=>'0');
   diag3SelAux <= (others=>'0');
   diag4SelAux <= (others=>'0');
  elsif WriteDiag1Select='1' then
   diag1SelAux <= DataFromDecoder;
  elsif WriteDiag2Select='1' then
   diag2SelAux <= DataFromDecoder;
  elsif WriteDiag3Select='1' then
   diag3SelAux <= DataFromDecoder;
  elsif WriteDiag4Select='1' then
   diag4SelAux <= DataFromDecoder;
  end if;
 end if;
end process Registers;

Diag1Mux: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  case diag1SelAux(2 downto 0) is
   when "000" =>
    diag1I <= RefI;
    diag1Q <= RefQ;
   when "001" =>
    diag1I <= FwdI;
    diag1Q <= FwdQ;
   when "010" =>
    diag1I <= CavI;
    diag1Q <= CavQ;
   when "011" =>
    diag1I <= ErrI;
    diag1Q <= ErrQ;
   when "100" =>
    diag1I <= OutI;
    diag1Q <= OutQ;
   when others =>
    diag1I <= (others=>'0');
    diag1Q <= (others=>'0');
   end case;
 end if;
end process Diag1Mux;

Diag2Mux: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  case diag2SelAux(2 downto 0) is
   when "000" =>
    diag2I <= RefI;
    diag2Q <= RefQ;
   when "001" =>
    diag2I <= FwdI;
    diag2Q <= FwdQ;
   when "010" =>
    diag2I <= CavI;
    diag2Q <= CavQ;
   when "011" =>
    diag2I <= ErrI;
    diag2Q <= ErrQ;
   when "100" =>
    diag2I <= OutI;
    diag2Q <= OutQ;
   when others =>
    diag2I <= (others=>'0');
    diag2Q <= (others=>'0');
   end case;
 end if;
end process Diag2Mux;

Diag3Mux: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  case diag3SelAux(2 downto 0) is
   when "000" =>
    diag3I <= RefI;
    diag3Q <= RefQ;
   when "001" =>
    diag3I <= FwdI;
    diag3Q <= FwdQ;
   when "010" =>
    diag3I <= CavI;
    diag3Q <= CavQ;
   when "011" =>
    diag3I <= ErrI;
    diag3Q <= ErrQ;
   when "100" =>
    diag3I <= OutI;
    diag3Q <= OutQ;
   when others =>
    diag3I <= (others=>'0');
    diag3Q <= (others=>'0');
   end case;
 end if;
end process Diag3Mux;

Diag4Mux: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  case diag4SelAux(2 downto 0) is
   when "000" =>
    diag4I <= RefI;
    diag4Q <= RefQ;
   when "001" =>
    diag4I <= FwdI;
    diag4Q <= FwdQ;
   when "010" =>
    diag4I <= CavI;
    diag4Q <= CavQ;
   when "011" =>
    diag4I <= ErrI;
    diag4Q <= ErrQ;
   when "100" =>
    diag4I <= OutI;
    diag4Q <= OutQ;
   when others =>
    diag4I <= (others=>'0');
    diag4Q <= (others=>'0');
   end case;
 end if;
end process Diag4Mux;

end RTL;
