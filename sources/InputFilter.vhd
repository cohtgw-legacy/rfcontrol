library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity InputFilter is
    Port ( X : in std_logic_vector(13 downto 0);
           Y : out std_logic_vector(13 downto 0);
           Clk : in std_logic);
end InputFilter;

architecture RTL of InputFilter is
constant a2: integer := 3405;
constant a3: integer := 2038;
constant b1: integer := 3858;
constant b2: integer := 7708;
constant b3: integer := 3858;

signal ykm1, ykm2, xk, xkm1, xkm2: signed(13 downto 0);
signal yAux1, yAux2, yAux3, yAux4, yAux5: signed (27 downto 0);
signal yk: signed (30 downto 0);

attribute syn_multstyle : string;
attribute syn_multstyle of yAux1 : signal is "block_mult";
attribute syn_multstyle of yAux2 : signal is "block_mult";
attribute syn_multstyle of yAux3 : signal is "block_mult";
attribute syn_multstyle of yAux4 : signal is "block_mult";
attribute syn_multstyle of yAux5 : signal is "block_mult";


begin

xk <= signed(X);
Y <= std_logic_vector(yk(30 downto 17));

yAux1 <= b1 * xk;
yAux2 <= b2 * xkm1;
yAux3 <= b3 * xkm2;
yAux4 <= a2 * ykm1;
yAux5 <= a3 * ykm2;

ClockedProcess: process(Clk)
begin
  if Clk'event and Clk='1' then 
    ykm1 <= yk(27 downto 14);
    ykm2 <= ykm1;
    xkm1 <= xk;
    xkm2 <= xkm1;
    yk   <= resize(yAux1,yk'length) + resize(yAux2,yk'length) + resize(yAux3,yk'length) 
            - resize(yAux4,yk'length) - resize(yAux5,yk'length);
  end if;
end process ClockedProcess;

end RTL;
