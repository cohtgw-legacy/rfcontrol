library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

--  Uncomment the following lines to use the declarations that are
--  provided for instantiating Xilinx primitive components.
--library UNISIM;
--use UNISIM.VComponents.all;

entity IQModulator is
    Port ( ResetNA : in std_logic;
	        ConfigMode: in std_logic;
           Clk : in std_logic;
           I : in std_logic_vector(13 downto 0);
           Q : in std_logic_vector(13 downto 0);
           IQCount : in std_logic_vector(1 downto 0);
           WaveOut1 : out std_logic_vector(13 downto 0);
           WaveOut2 : out std_logic_vector(13 downto 0));
end IQModulator;

architecture RTL of IQModulator is

attribute syn_useioff : boolean;
attribute syn_useioff of RTL : architecture is true;

COMPONENT SineCosine
	PORT(
		Theta : IN std_logic_vector(1 downto 0);
		Clk : IN std_logic;          
		Sine1 : OUT std_logic_vector(13 downto 0);
		Cosine1 : OUT std_logic_vector(13 downto 0);
		Sine2 : OUT std_logic_vector(13 downto 0);
		Cosine2 : OUT std_logic_vector(13 downto 0)
		);
END COMPONENT;

signal i100, q100: signed(13 downto 0);
signal counter: unsigned(1 downto 0);
signal sine1, cosine1, sine2, cosine2: signed(13 downto 0);
signal sine1Aux, cosine1Aux, sine2Aux, cosine2Aux: std_logic_vector(13 downto 0);
signal yAux1, yAux2, yAux3, yAux4: signed (27 downto 0);
--signal yAux1, yAux2: signed(14 downto 0);
signal wave1Aux, wave2Aux: signed (28 downto 0);
signal wave1, wave2: signed (26 downto 0);
signal resetA: std_logic;

attribute syn_multstyle : string;
attribute syn_multstyle of yAux1 : signal is "block_mult";
attribute syn_multstyle of yAux2 : signal is "block_mult";
attribute syn_multstyle of yAux3 : signal is "block_mult";
attribute syn_multstyle of yAux4 : signal is "block_mult";

begin

sine1 <= signed(sine1Aux);
cosine1 <= signed(cosine1Aux);
sine2 <= signed(sine2Aux);
cosine2 <= signed(cosine2Aux);
resetA <= ConfigMode or (not ResetNA);

sync1: process(resetA, Clk)
begin
 if resetA='1' then
  i100 <= (others=>'0');
  q100 <= (others=>'0');
 elsif Clk'event and Clk='1' then
  i100 <= signed(I);
  q100 <= signed(Q);
 end if;
end process sync1;

sync2: process(Clk)
begin
 if Clk'event and Clk='1' then
  --yAux1 <= resize(cosine1,15) + to_signed(8192, 15);
  --yAux2 <= resize(cosine2,15) + to_signed(8192, 15);
  yAux1 <= i100 * cosine1;
  yAux2 <= q100 * sine1;
  yAux3 <= i100 * cosine2;
  yAux4 <= q100 * sine2;

  if wave1Aux >= to_signed(67108863, wave1Aux'length) then     -- 2^26-1
   wave1 <= (others => '1');
  elsif wave1Aux <= to_signed(-67108864, wave1Aux'length) then  -- -2^26
   wave1 <= (others => '0');
  else
   wave1 <= not(wave1Aux(26)) & wave1Aux(25 downto 0);
  end if;

  --wave1 <= resize(wave1Aux, wave1'length) +
  --         to_signed(268435456, wave1'length);

  --wave2 <= resize(wave2Aux, wave2'length) +
   --        to_signed(268435456, wave2'length);
  if wave2Aux >= to_signed(67108863, wave2Aux'length) then
   wave2 <= (others => '1');
  elsif wave2Aux <= to_signed(-67108864, wave2Aux'length) then
   wave2 <= (others => '0');
  else
   wave2 <= not(wave2Aux(26)) & wave2Aux(25 downto 0);
  end if;
 end if;
end process sync2;

wave1Aux <= resize(yAux1,wave1Aux'length) + 
           resize(yAux2, wave1Aux'length);
wave2Aux <=  resize(yAux3,wave2Aux'length) + 
           resize(yAux4, wave2Aux'length);
WaveOut1 <= std_logic_vector(wave1(26 downto 13));
WaveOut2 <= std_logic_vector(wave2(26 downto 13));

Count4: process(Clk)
begin
 if Clk'event and Clk='1' then
  case IQCount is
      when "00" =>
         counter <= "00";
      when "01" =>
         counter <= "01";
      when "11" =>
         counter <= "10";
      when "10" =>
         counter <= "11";
      -- For XST compliance
		when others =>
         counter <= "00";
   end case;
 end if;
end process Count4;

U0_SineCosine: SineCosine PORT MAP(
		Theta => std_logic_vector(counter),
		Clk => Clk,
		Sine1 => sine1Aux,
		Cosine1 => cosine1Aux,
		Sine2 => sine2Aux,
		Cosine2 => cosine2Aux
	);

end RTL;
