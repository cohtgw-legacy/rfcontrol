-- The purpose of the IQCounter entity is to generate a 2-bit count value
-- that will drive the IQ Demodulator logic to stay deterministic.
-- A leading edge of the Clk20 input always corresponds to an I sample.
-- This will be detected by the demodulator by decoding a value of "01"
-- in the IQCount signal.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity IQCounter is
    Port ( Clk20 : in std_logic;
           Clk80 : in std_logic;
			  Alarm : out std_logic;
           IQCount : out std_logic_vector(1 downto 0));
end IQCounter;

architecture RTL of IQCounter is

signal clk20d1, clk20d2, clk20Rising: std_logic;
signal iqCountAux: std_logic_vector(1 downto 0);

begin

IQCount <= iqCountAux;

clk20Rising <= clk20d1 and not clk20d2; 

ClockedProcess: process(Clk80)
begin
  if Clk80'event and Clk80='1' then
	clk20d1 <= Clk20;
	clk20d2 <= clk20d1;
	if clk20Rising='1' then
	 iqCountAux <= "11";
   elsif iqCountAux = "11" then
	 iqCountAux <= "10";
   elsif iqCountAux = "10" then
	 iqCountAux <= "00";
   elsif iqCountAux = "00" then
	 iqCountAux <= "01";
   elsif iqCountAux = "01" then
	 iqCountAux <= "11";
	end if;
  end if;
end process ClockedProcess;
   
AlarmProcess: process(Clk80)
begin
  if Clk80'event and Clk80='1' then
   if iqCountAux = "01" then
	 if clk20Rising='0' then
	  Alarm <= '1';
	 else
	  Alarm <= '0';
	 end if;
	end if;
  end if;
end process AlarmProcess;	
	
end RTL;
