library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity SatControl is
    Port ( Clk : in std_logic;
           IDataIn : in std_logic_vector(13 downto 0);
           QDataIn : in std_logic_vector(13 downto 0);
           ISatIn : in std_logic_vector(13 downto 0);
           QSatIn : in std_logic_vector(13 downto 0);
           Saturated : out std_logic;
           Iout : out std_logic_vector(13 downto 0);
           Qout : out std_logic_vector(13 downto 0));
end SatControl;

architecture RTL of SatControl is

signal iSquared, qSquared: signed(27 downto 0);
signal moduleSquared: signed(28 downto 0);
signal iSatAbs, qSatAbs: signed(13 downto 0);
signal ix1, ix2, ix3, ix8: signed(16 downto 0);
signal ix3Aux: signed(27 downto 0);
signal qx1, qx2, qx3, qx8: signed(16 downto 0);
signal qx3Aux: signed(27 downto 0);
signal iOutAux, qOutAux: signed(13 downto 0);
signal iSign, qSign: std_logic;


begin

iSquared <= signed(IDataIn) * signed(IDataIn);
qSquared <= signed(QDataIn) * signed(QDataIn);
iSatAbs <= -signed(ISatIn) when ISatIn(13)='1' else signed(ISatIn);
qSatAbs <= -signed(QSatIn) when QSatIn(13)='1' else signed(QSatIn);
ix3Aux <= iSatAbs * to_signed(3, iSatAbs'length);
qx3Aux <= qSatAbs * to_signed(3, qSatAbs'length);
Iout <= std_logic_vector(iOutAux);
Qout <= std_logic_vector(qOutAux);


FirstStage: process(Clk)
begin
 if Clk'event and Clk='1' then
  moduleSquared <= resize(iSquared, moduleSquared'length) + resize(qSquared, moduleSquared'length);
  ix1 <= "000" & iSatAbs;
  ix2 <= "00" & iSatAbs & "0";
  ix3 <= ix3Aux(16 downto 0);
  ix8 <= iSatAbs & "000";
  qx1 <= "000" & qSatAbs;
  qx2 <= "00" & qSatAbs & "0";
  qx3 <= qx3Aux(16 downto 0);
  qx8 <= qSatAbs & "000";
  iSign <= ISatIn(13);
  qSign <= QSatIn(13);
 end if;
end process FirstStage;

SecondStage: process(Clk)
begin
 if Clk'event and Clk='1' then
  if moduleSquared > to_signed(67092481, moduleSquared'length) then     -- 8191^2
   Saturated <= '1';
  else
   Saturated <= '0';
  end if; 

  if (ix1 >= qx8) then
   if iSign='0' then
    iOutAux <= to_signed(8191, iOutAux'length);
   else 
    iOutAux <= to_signed(-8191, iOutAux'length);
   end if;
    qOutAux <= (others=>'0');
  elsif (qx8 > ix1) and (ix1 >= qx3) then
   if iSign='0' then
    iOutAux <= to_signed(7946, iOutAux'length);
   else 
    iOutAux <= to_signed(-7946, iOutAux'length);
   end if;
   if qSign='0' then
    qOutAux <= to_signed(1986, iOutAux'length);
   else 
    qOutAux <= to_signed(-1986, iOutAux'length);
   end if;
  elsif (qx3 > ix1) and (ix2 >= qx3) then
   if iSign='0' then
    iOutAux <= to_signed(7326, iOutAux'length);
   else 
    iOutAux <= to_signed(-7326, iOutAux'length);
   end if;
   if qSign='0' then
    qOutAux <= to_signed(3663, iOutAux'length);
   else 
    qOutAux <= to_signed(-3663, iOutAux'length);
   end if;
  elsif (qx3 > ix2) and (ix3 >= qx2) then
   if iSign='0' then
    iOutAux <= to_signed(5791, iOutAux'length);
   else 
    iOutAux <= to_signed(-5791, iOutAux'length);
   end if;
   if qSign='0' then
    qOutAux <= to_signed(5791, iOutAux'length);
   else 
    qOutAux <= to_signed(-5791, iOutAux'length);
   end if;
  elsif (qx2 > ix3) and (ix3 >= qx1) then
   if iSign='0' then
    iOutAux <= to_signed(3663, iOutAux'length);
   else 
    iOutAux <= to_signed(-3663, iOutAux'length);
   end if;
   if qSign='0' then
    qOutAux <= to_signed(7326, iOutAux'length);
   else 
    qOutAux <= to_signed(-7326, iOutAux'length);
   end if;
  elsif (qx1 > ix3) and (ix8 >= qx1) then
   if iSign='0' then
    iOutAux <= to_signed(1986, iOutAux'length);
   else 
    iOutAux <= to_signed(-1986, iOutAux'length);
   end if;
   if qSign='0' then
    qOutAux <= to_signed(7946, iOutAux'length);
   else 
    qOutAux <= to_signed(-7946, iOutAux'length);
   end if;
  else     -- (qx1 > ix8) 
   iOutAux <= (others=>'0');
   if qSign='0' then
    qOutAux <= to_signed(8191, iOutAux'length);
   else 
    qOutAux <= to_signed(-8191, iOutAux'length);
   end if;
  end if;
 end if;
end process SecondStage;

end RTL;
