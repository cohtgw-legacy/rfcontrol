library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity ResControl is
    Port ( Reset: in std_logic;
           Clk40 : in std_logic;
           WriteResCtrl: in std_logic;
			  WriteResCtrlTime: in std_logic;
			  ResCtrlTime: out std_logic_vector(15 downto 0);
			  CavI: in std_logic_vector(13 downto 0);
			  CavQ: in std_logic_vector(13 downto 0);
			  FwdI: in std_logic_vector(13 downto 0);
			  FwdQ: in std_logic_vector(13 downto 0);	
           RFOnRising: in std_logic;
           ResFwdI: out std_logic_vector(15 downto 0);
           ResFwdQ: out std_logic_vector(15 downto 0);
           ResCavI: out std_logic_vector(15 downto 0);
           ResCavQ: out std_logic_vector(15 downto 0);
           DataFromDecoder: in std_logic_vector(15 downto 0);
			  TimeTracker: out std_logic_vector(15 downto 0);
           RESCTRL: out std_logic_vector(15 downto 0);
           CSN: out std_logic;
           ResCTRLD: out std_logic;
           ResSClk: out std_logic
          );
end ResControl;

architecture RTL of ResControl is

type StateType is (Idle, Waiting, Counting);
signal currentState, nextState: StateType;
signal resCtrlAux, myShiftReg: std_logic_vector(15 downto 0);
signal resCtrlTimeAux: std_logic_vector(15 downto 0);
signal resCtrlCounter: unsigned(15 downto 0);
signal myDivider: unsigned(3 downto 0);
signal bitCounter: unsigned(4 downto 0);
signal csnAux, dataToDAC: std_logic;

begin

RESCTRL <= resCtrlAux;
ResCtrlTime <= resCtrlTimeAux;
TimeTracker <= std_logic_vector(resCtrlCounter);
ResCTRLD <= dataToDAC;
ResSClk <= std_logic(myDivider(3));
CSN <= csnAux;

currentState <= nextState;

FSM: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  if Reset='1' then
   nextState <= Idle;
	myDivider <= (others=>'0');
	bitCounter <= (others=>'0');
	myShiftReg <= (others=>'0');
	csnAux <= '1';
  else
   myDivider <= myDivider + 1;
   case currentState is
      when Idle =>
		 bitCounter <= (others=>'0');
		 csnAux <= '1';
       if WriteResCtrl='1' then
		  myShiftReg <= DataFromDecoder;
        nextState <= Waiting;
       end if;
		when Waiting => 
		 if myDivider=to_unsigned(0, myDivider'length) then
        nextState <= Counting;
		 end if;
      when Counting =>
		 csnAux <='0';
		 if myDivider=to_unsigned(4, myDivider'length) then
		  dataToDAC <= myShiftReg(15);
		  myShiftReg <= myShiftReg(14 downto 0) & '0';
		 end if;
		 if myDivider=to_unsigned(12, myDivider'length) then
		  bitCounter <= bitCounter + 1;
		 end if;
       if bitCounter=to_unsigned(16, bitCounter'length) then
		  nextState <= Idle;
		 end if;
      when others =>
       nextState <= Idle;
   end case; 
  end if;
 end if;
end process FSM;


RegProcess: process(Clk40)
begin
  if Clk40'event and Clk40='1' then
   if Reset='1' then
    resCtrlAux <= (others=>'0');
   elsif WriteResCtrl='1' then
    resCtrlAux <= DataFromDecoder;
   end if;
	if Reset='1' then
    resCtrlTimeAux <= (others=>'0');
   elsif WriteResCtrlTime='1' then
    resCtrlTimeAux <= DataFromDecoder;
   end if;
  end if;
end process RegProcess;

CountProcess: process(Clk40)
begin
  if Clk40'event and Clk40='1' then
   if RFOnRising='1' then
    resCtrlCounter <= (others=>'0');
   else
	 if resCtrlCounter < X"FFFF" then
	  resCtrlCounter <= resCtrlCounter + 1;
	 end if;
   end if;
	if resCtrlCounter = unsigned(resCtrlTimeAux) then
	 ResFwdI <= FwdI(13) & FwdI(13) & FwdI;
	 ResFwdQ <= FwdQ(13) & FwdQ(13) & FwdQ;
	 ResCavI <= CavI(13) & CavI(13) & CavI;
	 ResCavQ <= CavQ(13) & CavQ(13) & CavQ;
	end if;
  end if;
end process CountProcess;

end RTL;
