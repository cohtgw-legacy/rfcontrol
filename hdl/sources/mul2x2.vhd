-- This entity performs the rotation of I and Q data
-- This is just a matrix multiplication that produces the vector (IDataOut, QDataOut)
-- from the vector (IDataIn, QDataIn)
-- Only the outputs are clocked, since the inputs come from the synchronous IQDemod block.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity mul2x2 is
    Port ( IDataIn :  in std_logic_vector(13 downto 0);
           QDataIn :  in std_logic_vector(13 downto 0);
           WriteIA :  in std_logic;
           WriteIB :  in std_logic;
           WriteQA :  in std_logic;
           WriteQB :  in std_logic;
           IFactorA : out std_logic_vector(15 downto 0);
           IFactorB : out std_logic_vector(15 downto 0);
           QFactorA : out std_logic_vector(15 downto 0);
           QFactorB : out std_logic_vector(15 downto 0);
           DataFromDecoder: in std_logic_vector(15 downto 0);
           Clk40 :    in std_logic;
           Reset :    in std_logic;
           IDataOut : out std_logic_vector(13 downto 0);
           QDataOut : out std_logic_vector(13 downto 0));
end mul2x2;

architecture RTL of mul2x2 is

-- Signal declarations
signal ia, ib, qa, qb: signed(13 downto 0);
signal i, q, iOut, qOut: signed(13 downto 0);
signal iAux, qAux: signed(28 downto 0);

begin

IFactorA <= std_logic_vector(resize(ia, 16));
QFactorA <= std_logic_vector(resize(qa, 16));
IFactorB <= std_logic_vector(resize(ib, 16));
QFactorB <= std_logic_vector(resize(qb, 16));
iOut <= iAux(26 downto 13);   -- Considering the nature of the I and Q signals and the operations involved,
qOut <= qAux(26 downto 13);   -- bits 28 and 27 of qAux contain no useful information
IDataOut <= std_logic_vector(iOut);
QDataOut <= std_logic_vector(qOut);

ClockedProcess: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  i  <= signed(IDataIn);
  q  <= signed(QDataIn);
  iAux <= resize(i*ia, iAux'length) + resize(q*qa, iAux'length);
  qAux <= resize(i*ib, qAux'length) + resize(q*qb, qAux'length);
 end if;
end process ClockedProcess;

regProcess: process(Reset, Clk40)
begin
 if Reset='1' then
  ia <= to_signed(8191, ia'length);
  qa <= to_signed(0, qa'length);
  ib <= to_signed(0, ib'length);
  qb <= to_signed(8191, qb'length);
 elsif Clk40'event and Clk40='1' then
  if WriteIA='1' then
   ia <= signed(DataFromDecoder(13 downto 0));
  end if;
  if WriteQA='1' then
   qa <= signed(DataFromDecoder(13 downto 0));
  end if;
  if WriteIB='1' then
   ib <= signed(DataFromDecoder(13 downto 0));
  end if;
  if WriteQB='1' then
   qb <= signed(DataFromDecoder(13 downto 0));
  end if;
 end if;
end process regProcess;

end RTL;
