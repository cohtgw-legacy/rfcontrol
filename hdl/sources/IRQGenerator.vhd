library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity IRQGenerator is
    Port ( ResetNA : in std_logic;
           SyncReset : in std_logic;
           Clk40 : in std_logic;
           DataFromDecoder : in std_logic_vector(31 downto 0);
           WriteIENR : in std_logic;
           WriteISUR : in std_logic;
           ReadISCR : in std_logic;
           IRQ : in std_logic_vector(31 downto 0);
           ISCR: out std_logic_vector(31 downto 0);
           IENR: out std_logic_vector(31 downto 0);
           ISUR: out std_logic_vector(31 downto 0);
           VMEIntReqN : out std_logic;
           VMEIntProcessed : in std_logic;
           VMEIRQLevel : out std_logic_vector(2 downto 0);
           VMEIRQVector : out std_logic_vector(31 downto 0));
end IRQGenerator;

architecture RTL of IRQGenerator is

type StateType is (Idle, WaitForIRQ, Interrupt, ClearIRQ);

signal currentState, nextState: StateType;
signal irqSources: std_logic_vector(31 downto 0);
signal irqEnables: std_logic_vector(31 downto 0);
signal irqSetup: std_logic_vector(31 downto 0);

begin

ISCR <= irqSources;
IENR <= irqEnables;
ISUR <= irqSetup;
VMEIRQLevel <= "010" when irqSetup(8)='0' else "011";
VMEIRQVector <= "000000000000000000000000" 
                & irqSetup(7 downto 0);

States: process(ResetNA, Clk40)
begin
 if ResetNA='0' then
  currentState <= Idle;
 elsif Clk40'event and Clk40='1' then
  currentState <= nextState;
 end if; 
end process States;

Transitions: process(currentState, SyncReset, irqSources,
                     VMEIntProcessed, ReadISCR)
begin
 case currentState is 
  when Idle =>
   if SyncReset='1' then 
    nextState <= WaitForIRQ;
   else nextState <= Idle;
   end if;
  when WaitForIRQ =>
   if irqSources /= X"00000000" then 
    nextState <= Interrupt;
   else nextState <= WaitForIRQ;
   end if;
  when Interrupt =>
   if VMEIntProcessed='1' then 
    nextState <= ClearIRQ;
   else nextState <= Interrupt;
   end if;
  when ClearIRQ =>
   if ReadISCR='1' then 
    nextState <= WaitForIRQ;
   else nextState <= ClearIRQ;
   end if;
  when others =>
   nextState <= Idle;
 end case;
end process Transitions;

Outputs: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  case currentState is 
   when Idle =>
    VMEIntReqN <= '1';
   when WaitForIRQ =>
    VMEIntReqN <= '1';
   when Interrupt =>
    VMEIntReqN <= '0';
   when ClearIRQ =>
    VMEIntReqN <= '1';
   when others =>
    VMEIntReqN <= '1';
  end case;
 end if;
end process Outputs;

Sources: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  for I in 0 to 31 loop
   if SyncReset='1' then
    irqSources(I) <= '0';
   elsif irqEnables(I)='1' and IRQ(I)='1' then
    irqSources(I) <= '1';
   elsif ReadISCR='1' then
    irqSources(I) <= '0';
   end if;
  end loop;
 end if;
end process Sources;

Enables: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  if SyncReset='1' then
   irqEnables <= (others=>'0');
  elsif WriteIENR='1' then
   irqEnables <= DataFromDecoder;
  end if;
 end if;
end process Enables;

Setup: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  if SyncReset='1' then
   irqSetup <= (others=>'0');
  elsif WriteISUR='1' then
   irqSetup <= DataFromDecoder;
  end if;
 end if;
end process Setup;

end RTL;
