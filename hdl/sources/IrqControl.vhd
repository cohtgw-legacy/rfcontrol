----------------------------------------------------
--  
--  Unit Name :  IrqControl
--
-- Description:
--
-- The IrqControl entity contains all the logic related to VME interrupt generation.
-- It hosts the IRQ vector and IRQ sources registers, and it produces the (debounced) RFOnFalling and
-- RFOnRising signals for every other block to use. The interrupter is of the ROACK (Release on Acknowledge)
-- type. When the ISR comes and reads the IQR source register, it is cleared on read. There is a risk that
-- the ISR read clears a bit for which it was not invoked. The only side effect of this is that the next
-- invocation of the ISR will find an empty source register. This should be taken into account when writing
-- the ISR.
--
--  Author  :  Javier Serrano
--  Group   :  AB/CO 
--
--  Revisions:   
-- 	1.1. (24 September 2003) Initial release
-- 	1.4. (7 June 2008) Clean-up and comments.
--                  
--
--  For any bug or comment, please send an e-mail to Javier.Serrano@cern.ch

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity IrqControl is
    Port ( Clk : in std_logic;
           IrqEnable : in std_logic;
           RFOn : in std_logic;
			  StartCycleA: in std_logic;
           Reset : in std_logic;
           WriteVMEIRQR: in std_logic;
			  ReadIRQSRCR: in std_logic;
           DataFromDecoder: in std_logic_vector(15 downto 0);
           VMEIRQR: out std_logic_vector(15 downto 0);
			  IRQSRCR: out std_logic_vector(15 downto 0);
           IntProcessed : in std_logic;
           RFOnRising: out std_logic;
           RFOnFalling: out std_logic;
			  StartCycle: out std_logic;
           StateOut: out std_logic_vector(2 downto 0);
           IrqN : out std_logic);
end IrqControl;

architecture RTL of IrqControl is

type StateType is (Idle, Interrupting);
type DebounceStateType is (Idle, WaitingRFON, TempON, WaitingRFOFF, TempOFF);
type DebounceStateType2 is (Idle, Blocked);

attribute syn_enum_encoding : string;
attribute syn_enum_encoding of StateType : type is "sequential";
attribute syn_enum_encoding of DebounceStateType : type is "sequential";

signal rfOnFallingAux, rfOnRisingAux, rfOnD1, rfOnD2, rfOnD3: std_logic;
signal rfOnFallingOut, rfOnRisingOut: std_logic;
signal state, nextState: StateType;
signal debounceCurrent, debounceNext: DebounceStateType;
signal startDebounceCurrent, startDebounceNext: DebounceStateType2;
signal counter, counter2: unsigned(9 downto 0);
signal startCycleD1, startCycleD2, startCycleD3, startCycleRisingAux: std_logic;
signal startCycleRising: std_logic;
signal rfOnIRQSource, startCycleIRQSource: std_logic;

begin

rfOnFallingAux <= rfOnD3 and (not rfOnD2);
rfOnRisingAux <= rfOnD2 and (not rfOnD3);
startCycleRisingAux <= startCycleD2 and (not startCycleD3);
startCycleRising <= startCycleRisingAux when startDebounceCurrent=Idle else '0';
RFOnFalling <= rfOnFallingOut;
RFOnRising <= rfOnRisingOut;
IRQSRCR <= "00000000000000" & startCycleIRQSource & rfOnIRQSource;
StartCycle <= startCycleRising;

process(debounceCurrent)
begin
 case debounceCurrent is
  when Idle =>
   StateOut <= "000";
  when WaitingRFON =>
   StateOut <= "001";
  when TempON =>
   StateOut <= "010";
  when WaitingRFOFF =>
   StateOut <= "011";
  when TempOFF =>
   StateOut <= "100";
  when others =>
   StateOut <= "101";
 end case;
end process;

rfOnClocking: process(Clk)
begin
 if Clk'event and Clk='1' then
   rfOnD1 <= RFOn;
   rfOnD2 <= rfOnD1;
	rfOnD3 <= rfOnD2;
 end if;
end process rfOnClocking;

startCycleClocking: process(Clk)
begin
 if Clk'event and Clk='1' then
   startCycleD1 <= StartCycleA;
   startCycleD2 <= startCycleD1;
	startCycleD3 <= startCycleD2;
 end if;
end process startCycleClocking;

stateTransitions: process(Reset, Clk)
begin
 if Reset='1' then
  state <= Idle;
 elsif Clk'event and Clk='1' then
  state <= nextState;
 end if;
end process stateTransitions;

outputs: process(state, rfOnFallingOut, startCycleRising, IrqEnable, IntProcessed)
begin
   case state is
      when Idle =>
         IrqN <= '1';
         if (rfOnFallingOut='1' or startCycleRising='1') and IrqEnable='1' then
           nextState <= Interrupting;
         else nextState <= Idle;
         end if;
      when Interrupting =>
         IrqN <= '0';
         if IntProcessed = '1' then
           nextState <= Idle;
         else nextState <= Interrupting;
         end if;
      when others =>
         nextState <= Idle;
         IrqN <= '1';
   end case; 
end process outputs;

regProcess: process(Reset, Clk)
begin
 if Reset='1' then
  VMEIRQR <= (others=>'0');
 elsif Clk'event and Clk='1' then
  if WriteVMEIRQR='1' then
   VMEIRQR <= DataFromDecoder;
  end if;
 end if;
end process regProcess;

IRQSources: process(Reset, Clk)
begin
 if Reset='1' then
   rfOnIRQSource <= '0';
	startCycleIRQSource <= '0';
 elsif Clk'event and Clk='1' then
  if rfOnFallingOut='1' and IrqEnable='1' then
   rfOnIRQSource <= '1';
  elsif ReadIRQSRCR='1' then
   rfOnIRQSource <= '0';
  end if;
  if startCycleRising='1' and IrqEnable='1' then
   startCycleIRQSource <= '1';
  elsif ReadIRQSRCR='1' then
   startCycleIRQSource <= '0';
  end if;
 end if;
end process IRQSources;


debounceCurrent <= Idle when Reset='1' else debounceNext;

DebounceTransitions: process(Clk)
begin
 if Clk'event and Clk='1' then
  case debounceCurrent is 
   when Idle =>
    rfOnFallingOut <= '0';
    rfOnRisingOut <= '0';
    counter <= (others=>'0');
    debounceNext <= WaitingRFON;
   when WaitingRFON =>
    counter <= (others=>'0');
    if rfOnRisingAux='1' then
     rfOnRisingOut <= '1';
     debounceNext <= TempON;
    end if;
   when TempON =>
     rfOnRisingOut <= '0';
     rfOnFallingOut <= '0';
     if counter=to_unsigned(1000, counter'length) and RFOn='1' then
      debounceNext <= WaitingRFOFF;
     elsif counter=to_unsigned(1000, counter'length) and RFOn='0' then
      debounceNext <= WaitingRFON;
     else
      counter <= counter + 1;
     end if;
   when WaitingRFOFF =>
    counter <= (others=>'0');
    if rfOnFallingAux='1' then
     rfOnFallingOut <= '1';
     debounceNext <= TempOFF;
    end if;
   when TempOFF =>
     rfOnRisingOut <= '0';
     rfOnFallingOut <= '0';
     if counter=to_unsigned(1000, counter'length) and RFOn='1' then
      debounceNext <= WaitingRFOFF;
     elsif counter=to_unsigned(1000, counter'length) and RFOn='0' then
      debounceNext <= WaitingRFON;
     else
      counter <= counter + 1;
     end if;
   when others =>
     counter <= (others=>'0');
     rfOnRisingOut <= '0';
     rfOnFallingOut <= '0';
     debounceNext <= Idle;
  end case;
 end if;
end process DebounceTransitions;

startDebounceCurrent <= Idle when Reset='1' else startDebounceNext;

StartDebounceTransitions: process(Clk)
begin
 if Clk'event and Clk='1' then
  case startDebounceCurrent is 
   when Idle =>
    counter2 <= (others=>'0');
	 if startCycleRisingAux='1' then
     startDebounceNext <= Blocked;
	 end if;
   when Blocked =>
    counter2 <= counter2 + 1;
	 if counter2=to_unsigned(1000, counter2'length) then
     startDebounceNext <= Idle;
	 end if;
   when others =>
    counter2 <= (others=>'0');
	 startDebounceNext <= Idle;
  end case;
 end if;
end process StartDebounceTransitions;

end RTL;
