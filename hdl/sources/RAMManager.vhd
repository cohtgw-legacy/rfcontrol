library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity RAMManager is
    Port ( Clk40 : in std_logic;
           SyncReset : in std_logic;
           --Interface with VME Decoder
           VMEWriteRAM : in std_logic;
           VMEReadRAM : in std_logic;
           AddrFromDecoder : in std_logic_vector(18 downto 0);
           DataFromDecoder : in std_logic_vector(15 downto 0);
           RMDataToDecoder: out std_logic_vector(15 downto 0);
           RMDataToDecoderValid : out std_logic;
           -- RAMSELECT register
           WriteRAMSELECT : in std_logic;
           RAMSELECT: out std_logic_vector(15 downto 0);
           -- Interface to RAM
           RMDataToRAM : out std_logic_vector(15 downto 0);
           RMAddrToRAM : out std_logic_vector(17 downto 0);
           -- SetPoint RAM
           ReadSPRAM : out std_logic;
           WriteSPRAM : out std_logic;
           SPDataToRM: in std_logic_vector(15 downto 0);
           SPDataValid: in std_logic;
           -- FeedForward RAM
           ReadFFRAM : out std_logic;
           WriteFFRAM : out std_logic;
           FFDataToRM: in std_logic_vector(15 downto 0);
           FFDataValid: in std_logic;
           -- Diagnostics RAM
           DiagDataToRM: in std_logic_vector(15 downto 0);
           DiagDataValid: in std_logic;
           ReadDIAG1RAM : out std_logic;
           ReadDIAG2RAM : out std_logic;
           ReadDIAG3RAM : out std_logic;
           ReadDIAG4RAM : out std_logic);
end RAMManager;

architecture RTL of RAMManager is

signal ramSelectAux: std_logic_vector(15 downto 0); 
signal ramAccess: std_logic;

begin

RAMSELECT <= ramSelectAux;
ramAccess <= VMEWriteRAM or VMEReadRAM;

AddrDataClocking: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  if ramAccess='1' then
   RMAddrToRAM <= AddrFromDecoder(17 downto 0);
   RMDataToRAM <= DataFromDecoder;
  end if;
 end if;
end process AddrDataClocking;

StrobeGen: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  if ramSelectAux(2 downto 0)="000" and VMEWriteRAM='1' then
   WriteSPRAM <= '1';
  else 
   WriteSPRAM <= '0';
  end if;
  if ramSelectAux(2 downto 0)="000" and VMEReadRAM='1' then
   ReadSPRAM <= '1';
  else 
   ReadSPRAM <= '0';
  end if;
  if ramSelectAux(2 downto 0)="001" and VMEWriteRAM='1' then
   WriteFFRAM <= '1';
  else 
   WriteFFRAM <= '0';
  end if;
  if ramSelectAux(2 downto 0)="001" and VMEReadRAM='1' then
   ReadFFRAM <= '1';
  else 
   ReadFFRAM <= '0';
  end if;
  if ramSelectAux(2 downto 0)="010" and VMEReadRAM='1' then
   ReadDIAG1RAM <= '1';
  else 
   ReadDIAG1RAM <= '0';
  end if;
  if ramSelectAux(2 downto 0)="011" and VMEReadRAM='1' then
   ReadDIAG2RAM <= '1';
  else 
   ReadDIAG2RAM <= '0';
  end if;
  if ramSelectAux(2 downto 0)="100" and VMEReadRAM='1' then
   ReadDIAG3RAM <= '1';
  else 
   ReadDIAG3RAM <= '0';
  end if;
  if ramSelectAux(2 downto 0)="101" and VMEReadRAM='1' then
   ReadDIAG4RAM <= '1';
  else 
   ReadDIAG4RAM <= '0';
  end if;
 end if;
end process StrobeGen;

VMERead: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  if SPDataValid='1' then
   RMDataToDecoder <= SPDataToRM;
   RMDataToDecoderValid <= '1';
  elsif FFDataValid='1' then
   RMDataToDecoder <= FFDataToRM;
   RMDataToDecoderValid <= '1';
  elsif DiagDataValid='1' then
   RMDataToDecoder <= DiagDataToRM;
   RMDataToDecoderValid <= '1';
  else 
   RMDataToDecoderValid <= '0';
  end if;
 end if;
end process VMERead;

RamSelectProcess: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  if SyncReset='1' then
   ramSelectAux <= (others=>'0');
  elsif WriteRAMSELECT='1' then
   ramSelectAux <= DataFromDecoder;
  end if;
 end if;
end process RamSelectProcess;

end RTL;
