library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Snapshots is
    Port ( SyncReset : in std_logic;
           Clk40 : in std_logic;
           DataFromDecoder : in std_logic_vector(15 downto 0);
           WriteDiagTime : in std_logic;
           DiagTime : out std_logic_vector(15 downto 0);
           RFOnRising : in std_logic;
           RFOnFalling : in std_logic;
           ConfigMode: in std_logic;
			  TimeTracker: in std_logic_vector(15 downto 0);
           RFOffTime : out std_logic_vector(15 downto 0);
           RefIIn : in std_logic_vector(13 downto 0);
           RefIOut : out std_logic_vector(15 downto 0);
           RefQIn : in std_logic_vector(13 downto 0);
           RefQOut : out std_logic_vector(15 downto 0);
           FwdIIn : in std_logic_vector(13 downto 0);
           FwdIOut : out std_logic_vector(15 downto 0);
           FwdQIn : in std_logic_vector(13 downto 0);
           FwdQOut : out std_logic_vector(15 downto 0);          
           CavIIn : in std_logic_vector(13 downto 0);
           CavIOut : out std_logic_vector(15 downto 0);
           CavQIn : in std_logic_vector(13 downto 0);
           CavQOut : out std_logic_vector(15 downto 0);
           ErrIIn : in std_logic_vector(13 downto 0);
           ErrIOut : out std_logic_vector(15 downto 0);
           ErrQIn : in std_logic_vector(13 downto 0);
           ErrQOut : out std_logic_vector(15 downto 0);
           IntIIn : in std_logic_vector(13 downto 0);
           IntIOut : out std_logic_vector(15 downto 0);
           IntQIn : in std_logic_vector(13 downto 0);
           IntQOut : out std_logic_vector(15 downto 0);
           PropIIn : in std_logic_vector(13 downto 0);
           PropIOut : out std_logic_vector(15 downto 0);
           PropQIn : in std_logic_vector(13 downto 0);
           PropQOut : out std_logic_vector(15 downto 0);
           OutIIn : in std_logic_vector(13 downto 0);
           OutIOut : out std_logic_vector(15 downto 0);
           OutQIn : in std_logic_vector(13 downto 0);
           OutQOut : out std_logic_vector(15 downto 0));
end Snapshots;

architecture RTL of Snapshots is
signal diagTimeAux: std_logic_vector(15 downto 0);

begin

DiagTime <= diagTimeAux;

DiagTimeProcess: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  if SyncReset='1' then
   diagTimeAux <= X"00FF"; -- for example
  elsif WriteDiagTime='1' then
   diagTimeAux <= DataFromDecoder;
  end if;
 end if;
end process DiagTimeProcess;


Logging: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  if TimeTracker=diagTimeAux then
   RefIOut <= RefIIn(13) & RefIIn(13) & RefIIn;
   RefQOut <= RefQIn(13) & RefQIn(13) & RefQIn;
   FwdIOut <= FwdIIn(13) & FwdIIn(13) & FwdIIn;
   FwdQOut <= FwdQIn(13) & FwdQIn(13) & FwdQIn;
   CavIOut <= CavIIn(13) & CavIIn(13) & CavIIn;
   CavQOut <= CavQIn(13) & CavQIn(13) & CavQIn;
   ErrIOut <= ErrIIn(13) & ErrIIn(13) & ErrIIn;
   ErrQOut <= ErrQIn(13) & ErrQIn(13) & ErrQIn;
   IntIOut <= IntIIn(13) & IntIIn(13) & IntIIn;
   IntQOut <= IntQIn(13) & IntQIn(13) & IntQIn;
   PropIOut <= PropIIn(13) & PropIIn(13) & PropIIn;
   PropQOut <= PropQIn(13) & PropQIn(13) & PropQIn;
   OutIOut <= OutIIn(13) & OutIIn(13) & OutIIn;
   OutQOut <= OutQIn(13) & OutQIn(13) & OutQIn;
  end if;
  if RFOnFalling='1' then
   RFOffTime <= TimeTracker;
  end if;
 end if;
end process Logging;


end RTL;
