----------------------------------------------------
--  
--  Unit Name :  ClockManager
--
-- Description:
--
-- The ClockManager entity generates all the internal clocks for the design.
-- The master clock is the 40 MHz coming from outside of the card
-- We then generate a 40 MHz, 80 MHz, and 80 MHz negated.
-- The lock signals are also available for debugging.
--
--  Author  :     Javier Serrano
--  Group:     AB/CO 
--
--  Revisions:   
-- 	1.1. (24 September 2003) Initial release
-- 	1.5. (2 June 2008) Clean-up and comments.
--                     
--
--  For any bug or comment, please send an e-mail to Javier.Serrano@cern.ch


library IEEE;
use IEEE.std_logic_1164.all;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;


entity ClockManager is
  port ( 
      Reset         :  in std_logic;
      Clk40         :  in std_logic;      
      Clk40Int      :  out std_logic; 
      Clk80Int      :  out std_logic;
      Clk80IntN     :  out std_logic;
      Lock40Int     :  out std_logic
       );
end ClockManager;

architecture RTL of ClockManager is

-- Signal Declarations:
signal GND                  : std_logic;
--
signal clk40Aux, clk40x2    : std_logic;
signal clk80FBAux           : std_logic;
signal clk40FBAux           : std_logic;
signal clk40ExtAux          : std_logic;
signal clk80IntAux          : std_logic;
signal clk80ExtAux          : std_logic;
signal clk40IntAux          : std_logic;
signal clk80IntNAux         : std_logic;


begin
GND <= '0';
--
Clk40Int  <= clk40FBAux;
Clk80Int  <= clk80IntAux;

clk40Aux <= Clk40;

U0_DCM : DCM
   generic map (
      CLKIN_PERIOD => 25.0,          --  Specify period of input clock
      CLK_FEEDBACK => "1X",         --  Specify clock feedback of NONE, 1X or 2X
      DLL_FREQUENCY_MODE => "LOW",     --  HIGH or LOW frequency mode for DLL
      DUTY_CYCLE_CORRECTION => TRUE, --  Duty cycle correction, TRUE or FALSE
      STARTUP_WAIT => FALSE) --  Delay configuration DONE until DCM LOCK, TRUE/FALSE
   port map (
      CLK0 => clk40IntAux,     -- 0 degree DCM CLK ouptput
      CLK2X => clk40x2,   -- 2X DCM CLK output
      CLK2X180 => clk80IntNAux, -- 2X, 180 degree DCM CLK out
      LOCKED => Lock40Int, -- DCM LOCK status output
      CLKFB => clk40FBAux,   -- DCM clock feedback
      CLKIN => clk40Aux,   -- Clock input (from IBUFG, BUFG or DCM)
      PSCLK => GND,   -- Dynamic phase adjust clock input
      PSEN => GND,     -- Dynamic phase adjust enable input
      PSINCDEC => GND, -- Dynamic phase adjust increment/decrement
      RST => Reset        -- DCM asynchronous reset input
   );

-- BUFG Instantiation for Clk80Int
U0_BUFG: BUFG
  port map (
      I => clk40x2,
      O => clk80IntAux
  	   );
--

-- BUFG Instantiation for Clk40Int
U1_BUFG: BUFG
  port map (
      I => clk40IntAux,
      O => Clk40FBAux
  	   );
--

-- BUFG Instantiation for Clk80IntN
U2_BUFG: BUFG
  port map (
      I => clk80IntNAux,
      O => Clk80IntN
  	   );



end RTL;


