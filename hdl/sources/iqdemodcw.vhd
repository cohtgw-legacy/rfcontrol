-- The IQDemod entity takes a stream of I, Q, -I, -Q data at 80 MSPS and produces
-- two streams of 40 MSPS each. One contains I, I, I, ... data and the other one
-- contains Q, Q, Q, ... data. To do this, IQDemod relies on the IQCount value output
-- by IQCounter for sequencing. 
-- It then negates (2's complement) every other sample and demultiplexes the result to
-- separate I from Q.
-- Input and outputs are all clocked.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity IQDemodCW is
    Port ( DataInA : in std_logic_vector(13 downto 0);
           Clk80In : in std_logic;
			  IQCount : in std_logic_vector(1 downto 0);
           IData   : out std_logic_vector(13 downto 0);
           QData   : out std_logic_vector(13 downto 0));
end IQDemodCW;

architecture RTL of IQDemodCW is

signal clockedData: signed(13 downto 0);
signal iqCount0, iqCount1, demuxCtrl: std_logic;
signal convertedData, dataToDemux: signed(13 downto 0);
signal offsetI, offsetQ: signed(13 downto 0);
signal offsetIAux, offsetQAux, correctedData: signed(13 downto 0);
signal plusI, plusQ, minusI, minusQ: signed(13 downto 0);

begin

iqCount0 <= IQCount(0);  -- for convenience
iqCount1 <= IQCount(1);  -- for convenience
demuxCtrl <= iqCount0 xor iqCount1;

-- Process ClockData clocks the 14 bits coming from the ADC in the FPGA.
-- The flip-flops used here should be of the IFD type for best performance.
-- This process is also used to clock any other signals that need it inside the design.
ClockData: process(Clk80In)
begin
  if Clk80In'event and Clk80In='1' then
   clockedData <= signed(DataInA);
	dataToDemux <= convertedData;
  end if;
end process ClockData;

-- Process SignConversion translates the stream of i, q, -i, -q, ...
-- into a stream of i, q, i, q, ...
SignConversion: process(iqCount0, correctedData)
begin
  if iqCount0='0' then 
   convertedData <= -correctedData;
  else convertedData <= correctedData;
  end if;
end process SignConversion;

-- The Demux process divides an 80 MSPS stream of i, q, i, q, ...
-- into two 40 MSPS streams of i, i, i, ... and q, q, q, ...
Demux: process(Clk80In)
begin
  if Clk80In'event and Clk80In='1' then
   if demuxCtrl = '1' then
	  QData <= std_logic_vector(dataToDemux);
	else IData <= std_logic_vector(dataToDemux);
	end if;
  end if;
end process Demux;

Compensator: process(Clk80In)
begin
 if Clk80In'event and Clk80In='1' then
  case IQCount is
   when "00" =>
    minusQ <= clockedData;    
   when "01" =>
    plusI <= clockedData;
   when "11" =>
    plusQ <= clockedData;   
   when "10" =>
    minusI <= clockedData;
	-- For XST compliance 
   when others =>
    minusQ <= (others=>'0');     
   end case;
   offsetIAux <= plusI + minusI;
   offsetQAux <= plusQ + minusQ;
 end if;
end process Compensator;

offsetI <=  offsetIAux(13) & offsetIAux(13 downto 1);
offsetQ <=  offsetQAux(13) & offsetQAux(13 downto 1);

correctedData <= clockedData;

end RTL;
