library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

--  Uncomment the following lines to use the declarations that are
--  provided for instantiating Xilinx primitive components.
--library UNISIM;
--use UNISIM.VComponents.all;

entity LedsTest is
    Port ( ResetNA : in std_logic;
           Clk : in std_logic;
           Leds : out std_logic_vector(4 downto 0));
end LedsTest;

architecture RTL of LedsTest is
signal counter: unsigned(24 downto 0);

begin

Leds <= std_logic_vector (counter(24 downto 20));

process(ResetNA, Clk)
begin

 if ResetNA='0' then
  counter <= (others=>'0');
 elsif Clk'event and Clk='1' then
  counter <= counter + 1;
 end if;

end process;

end RTL;
