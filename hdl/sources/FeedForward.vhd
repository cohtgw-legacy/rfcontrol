library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity FeedForward is
    Port ( Clk40 : in std_logic;
           SyncReset : in std_logic;
           DataIn : in std_logic_vector(13 downto 0);
           DataOut : out std_logic_vector(13 downto 0);
           RFONFalling : in std_logic;
           RFONRising: in std_logic;
           MainLoopSwitch: in std_logic;
           FFData : in std_logic_vector(13 downto 0);
           FFDataValid : in std_logic);
end FeedForward;

architecture RTL of FeedForward is

signal dataInSigned, ffDataSigned, dataOut14: signed(13 downto 0);
signal dataOut15: signed(14 downto 0);
signal rfOn: std_logic;

begin

dataInSigned <= signed(DataIn);
DataOut <= std_logic_vector(dataOut14);

DataInProcess: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  if SyncReset='1' or rfOn='0' then
   ffDataSigned <= (others=>'0');
  elsif FFDataValid='1' then
   ffDataSigned <= signed(FFData);
  end if;
 end if;
end process DataInProcess;

DataOutProcess: process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  if MainLoopSwitch='1' then -- switch is open
   dataOut15 <= resize(ffDataSigned, dataOut15'length);
  else
   dataOut15 <= resize(ffDataSigned, dataOut15'length) + 
                resize(dataInSigned, dataOut15'length);
  end if;
  if dataOut15 >= to_signed(8191, dataOut15'length) then
   dataOut14 <= to_signed(8191, dataOut14'length);
  elsif dataOut15 <= to_signed(-8192, dataOut15'length) then
   dataOut14 <= to_signed(-8192, dataOut14'length);
  else
   dataOut14 <= dataOut15(13 downto 0);
  end if; 
 end if;
end process DataOutProcess;

goRF:process(Clk40)
begin
 if Clk40'event and Clk40='1' then
  if RFONFalling='1' then
   rfOn <= '0';
  elsif RFONRising='1' then
   rfOn <= '1';
  end if;
 end if;
end process goRF;

end RTL;
