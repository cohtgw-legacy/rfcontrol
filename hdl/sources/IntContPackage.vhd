
--	Purpose: This package defines supplemental types, subtypes, 
--		 constants, and functions 


library IEEE;
use IEEE.STD_LOGIC_1164.all;

package IntContPackage is

  constant NUMREGS: integer := 23; -- Number of registers

  subtype IntDataType is std_logic_vector(15 downto 0);
  type RegsType is array (integer range 0 to NUMREGS-1) of IntDataType;  

end IntContPackage;



