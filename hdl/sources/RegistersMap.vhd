library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

package RegistersMap is

 -- Constants declaration

 constant IRQSRCR       : integer := 0;
 constant STATER        : integer := 1;
 constant CONTROLR      : integer := 2;
 constant VMEIRQR       : integer := 3;
 constant RAMSELECTR    : integer := 4;
 constant DIAG1SELECTR  : integer := 5;
 constant DIAG2SELECTR  : integer := 6;
 constant DIAG3SELECTR  : integer := 7;
 constant DIAG4SELECTR  : integer := 8;
 constant RESCTRLTIMER  : integer := 9;
 constant RESCTRLR      : integer :=10;
 constant RESFWDIR      : integer :=11;
 constant RESFWDQR      : integer :=12;
 constant RESCAVIR      : integer :=13;
 constant RESCAVQR      : integer :=14;
 constant SWITCHCTRLR   : integer :=15;
 constant SOFTSWITCHR   : integer :=16;
 constant REFMATRIXAR   : integer :=17;
 constant REFMATRIXBR   : integer :=18;
 constant REFMATRIXCR   : integer :=19;
 constant REFMATRIXDR   : integer :=20;
 constant FWDMATRIXAR   : integer :=21;
 constant FWDMATRIXBR   : integer :=22;
 constant FWDMATRIXCR   : integer :=23;
 constant FWDMATRIXDR   : integer :=24;
 constant CAVMATRIXAR   : integer :=25;
 constant CAVMATRIXBR   : integer :=26;
 constant CAVMATRIXCR   : integer :=27;
 constant CAVMATRIXDR   : integer :=28;
 constant DIAGTIMER     : integer :=29;
 constant REFDIAGIR     : integer :=30;
 constant REFDIAGQR     : integer :=31;
 constant FWDDIAGIR     : integer :=32;
 constant FWDDIAGQR     : integer :=33;
 constant CAVDIAGIR     : integer :=34;
 constant CAVDIAGQR     : integer :=35;
 constant ERRDIAGIR     : integer :=36;
 constant ERRDIAGQR     : integer :=37;
 constant OUTDIAGIR     : integer :=38;
 constant OUTDIAGQR     : integer :=39;
 constant POUTDIAGIR    : integer :=40;
 constant POUTDIAGQR    : integer :=41;
 constant IOUTDIAGIR    : integer :=42;
 constant IOUTDIAGQR    : integer :=43;
 constant KPR           : integer :=44;
 constant KIR           : integer :=45;
 constant RFOFFTIMER    : integer :=46;
 constant PULSENUMBERR  : integer :=47;
 constant NEXTCYCLER    : integer :=48;
 constant PRESENTCYCLER : integer :=49;
 constant VHDLVERHR     : integer :=50;
 constant VHDLVERLR     : integer :=51;
 constant STATUSR       : integer :=52;
 constant RFONMAXLENGTHR: integer :=53;

 constant NumberOfRegs  : integer :=54;

 constant IntAddrWidth  : integer := 19;

 constant StartRegs : unsigned(IntAddrWidth-1 downto 0):= to_unsigned(0, IntAddrWidth);
 constant StopRegs  : unsigned(IntAddrWidth-1 downto 0):= to_unsigned(NumberOfRegs-1, IntAddrWidth);
 constant StartRAM  : unsigned(IntAddrWidth-1 downto 0):= to_unsigned(262144, IntAddrWidth);
 constant StopRAM   : unsigned(IntAddrWidth-1 downto 0):= to_unsigned(524287, IntAddrWidth);

 -- Types declaration

 type RegsType is array (0 to NumberOfRegs-1) of std_logic_vector(15 downto 0);

end RegistersMap;
