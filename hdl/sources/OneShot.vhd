library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity OneShot is
    Port ( Clk : in std_logic;
           ResetNA: in std_logic;
           PulseIn : in std_logic;
           PulseOut : out std_logic);
end OneShot;

architecture RTL of OneShot is

type StateType is (Idle, Counting);
signal currentState, nextState: StateType;
signal myCounter: unsigned(21 downto 0);

begin

stateTransitions: process(ResetNA, Clk)
begin
 if ResetNA='0' then
  currentState <= Idle;
 elsif Clk'event and Clk='1' then
  currentState <= nextState;
 end if;
end process stateTransitions;

Outputs: process(Clk)
begin
 if Clk'event and Clk='1' then
   case currentState is
      when Idle =>
       myCounter <= (others=>'0');
       PulseOut <= '0';
       if PulseIn='1' then
        nextState <= Counting;
       end if;
      when Counting =>
       myCounter <= myCounter + 1;
       PulseOut <= '1';
       if myCounter=to_unsigned(4000000, myCounter'length) then
        nextState <= Idle;
       end if;
      when others =>
       nextState <= Idle;
       myCounter <= (others=>'0');
       PulseOut <= '0';
   end case; 
 end if;
end process Outputs;

end RTL;
